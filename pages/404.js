import NotFound from "../src/components/NotFound";

export default function NotFoundPage(props) {
  return <NotFound />;
}
