import ItemPage from "../../src/pages/ItemPage";
import { getSEOProduct, getSEOProductByRef } from "../../src/store/actions/SEOActions";

export default props => {
	return <ItemPage SEO={props.SEOData} />;
};

export const getServerSideProps = async context => {
	const { productID } = context.query;

	let SEOData = await getSEOProductByRef(productID);

	return { props: { SEOData } };
};
