import Business from "../src/pages/Business";
import { getSEOBusiness, getSEOBusinessBysubdomain } from "../src/store/actions/SEOActions";
import { storeSubdomain } from "../src/utils/key";

export default props => {
	return <Business SEO={props.SEOData} />;
};

export const getServerSideProps = async context => {
	let host = new String(context.req.headers.host);

	let isOld = host.includes("dbot.store");
	let isProd = host.includes("storeup.site") || isOld;
	let bizID = isProd ? host.split(isOld ? "dbot.store" : "storeup.site")[0] : `${storeSubdomain}.`;
	bizID = bizID.replace(".", "");

	let SEOData = await getSEOBusinessBysubdomain(bizID);
	// let SEOData = {
	// 	subdomain: bizID,
	// };

	return { props: { SEOData } };
};
