import { getAllStoreWithListings } from "../../src/store/actions/businessAction";
import isEmpty from "../../src/utils/isEmpty";

const getAllPages = async () => {
	let pages = [];

	// GET STATIC PAGES
	// const staticPage = await import(`../../sitemap.json`);
	// staticPage.routes.map((e) => {
	//   pages.push(e);
	// });

	//GET DYNAMIC PAGES
	const dynamicPage = await getAllStoreWithListings();
	dynamicPage.map(s => {
		pages.push(`https://storeup.io/${s.subdomain}`);

		s.products,
			map((e, i) => {
				pages.push(`https://storeup.io/${s.subdomain}/p/${e.ref}`);
			});

		// s.listings.map((l, i) => {
		// 	const title = encodeURIComponent(l.item_title.replace(/ /gi, "-"));
		// 	pages.push(`https://${s.store_name}.storeup.site/product/${l.item_id}/${title}`);

		// 	if (i + 1 === s.listings.length) {
		// 		pages.push(`https://${s.store_name}.storeup.site`);
		// 	}
		// });
	});

	return pages;
};

export default async (req, res) => {
	try {
		//Get all the data for dynamic sitemap generation
		const pages = await getAllPages();

		//Generate sitemap
		const urlSet = pages
			.map(link => {
				// Build url portion of sitemap.xml
				return `<url><loc>${link}</loc></url>`;
				// return `<url><loc>https://${domain}${link}</loc></url>`;
			})
			.join("");

		// Add urlSet to entire sitemap string
		const sitemap = `<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">${urlSet}</urlset>`;

		// set response content header to xml
		res.setHeader("Content-Type", "text/xml");
		// write the sitemap
		res.write(sitemap);
		res.end();
	} catch (err) {
		console.error(err);
		res.status(500).send({ message: "Server Error" });
	}
};
