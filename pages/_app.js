import React from "react";
import { Provider } from "react-redux";
import PropTypes from "prop-types";
import Head from "next/head";
import App from "next/app";
import { ThemeProvider } from "@material-ui/core/styles";
import { theme } from "../src/theme";
import store from "../src/store";
import Layout from "../src/components/layout/PublicRoute";
import "../src/index.css";
import "../src/dayPicker.css";
import isEmpty from "../src/utils/isEmpty";
import { storeSubdomain } from "../src/utils/key";

const MyApp = props => {
	const { Component, pageProps } = props;

	React.useEffect(() => {
		// Remove the server-side injected CSS.
		const jssStyles = document.querySelector("#jss-server-side");
		if (jssStyles) {
			jssStyles.parentElement.removeChild(jssStyles);
		}
	}, []);

	return (
		<React.Fragment>
			<Head>
				<title>StoreUp</title>
				<meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
				<link rel="preconnect" href="https://fonts.googleapis.com" />
				<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
				<link
					href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap"
					rel="stylesheet"
				/>
				<link
					rel="stylesheet"
					href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
					integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm"
					crossorigin="anonymous"
				/>
				<link
					href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
					rel="stylesheet"
				/>
			</Head>

			<Provider store={store}>
				<ThemeProvider theme={theme}>
					<Layout pageProps={pageProps}>
						<Component {...pageProps} />
					</Layout>
				</ThemeProvider>
			</Provider>
		</React.Fragment>
	);
};

MyApp.getInitialProps = async appContext => {
	// calls page's `getInitialProps` and fills `appProps.pageProps`
	let appProps = await App.getInitialProps(appContext);

	if (appContext.ctx.req) {
		let host = new String(appContext.ctx.req.headers.host);

		let isOld = host.includes("dbot.store");
		let isProd = host.includes("storeup.site") || isOld;
		let bizID = isProd ? host.split(isOld ? "dbot.store" : "storeup.site")[0] : `${storeSubdomain}.`;

		bizID = bizID.replace(".", "");
		if (isEmpty(bizID)) {
			appContext.ctx.res.writeHead(302, {
				Location: `https://storeup.io`,
			});
			appContext.ctx.res.end();
			return;
		}
		if (isOld) {
			appContext.ctx.res.writeHead(302, {
				Location: `https://${bizID}.storeup.site`,
			});
			appContext.ctx.res.end();
		}
		// console.log(host, host.includes("storeup.site"));

		appProps = {
			...appProps,
			pageProps: {
				...appProps.pageProps,
				// bizID: appContext.ctx.query.bizID,
				bizID,
				pathname: appContext.ctx.asPath,
				query: appContext.ctx.query,
			},
		};
	}

	return appProps;
};

MyApp.propTypes = {
	Component: PropTypes.elementType.isRequired,
	pageProps: PropTypes.object.isRequired,
};

export default MyApp;
