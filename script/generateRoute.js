const fs = require("fs");

const generateRoute = () => {
  let link = [];

  // get all static pages
  const staticPage = fs.readdirSync(`${process.cwd()}/pages`, "utf-8");
  staticPage
    .filter((fn) => fn.endsWith(".js"))
    .map((e) => {
      if (e.startsWith("_") || e.includes("404")) {
      } else {
        let s = e.split(".js")[0];
        s = s === "index" ? "" : s;

        // link.push(`/${s}`);
        if (s.length > 0) {
          link.push(`/${s}`);
        }
      }
    });

  let l = JSON.stringify(link);
  const str = `{"routes":${l}}`;

  // Create sitemap file
  fs.writeFileSync("sitemap.json", str);
};

module.exports = generateRoute;
