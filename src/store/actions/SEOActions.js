import axios from "axios";
import { APILINK, XANO_APILINK } from "../../utils/key";
import { getError } from "./errorAction";

export const getSEOBusiness = async subdomain => {
	let data = {};
	await axios
		.get(`${APILINK}/v1/seo/business/${subdomain}`)
		.then(async res => {
			data = res.data.business;
		})
		.catch(err => {
			getError(err);
		});

	return data;
};

export const getSEOProduct = async id => {
	let data = {};
	await axios
		.get(`${APILINK}/v1/seo/item/${id}`)
		.then(async res => {
			console.log(res.data);
			data = res.data.item;
		})
		.catch(err => {
			getError(err);
		});

	return data;
};

export const getSEOProductByRef = async product_ref => {
	let data = {};
	await axios
		.get(`${XANO_APILINK}/product/${product_ref}`, {
			headers: { "X-Data-Source": "test" },
		})
		.then(async res => {
			// console.log(res.data);
			data = res.data.product;
		})
		.catch(err => {
			getError(err);
		});

	return data;
};

export const getSEOBusinessBysubdomain = async subdomain => {
	let data = {};
	await axios
		.get(`${XANO_APILINK}/business?subdomain=${subdomain}`, {
			headers: { "X-Data-Source": "test" },
		})
		.then(async res => {
			data = res.data;
		})
		.catch(err => {
			getError(err);
		});

	return data;
};
