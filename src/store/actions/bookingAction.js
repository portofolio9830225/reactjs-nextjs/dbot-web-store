import axios from "axios";

import { setLoading } from "./loadingAction";
import { XANO_APILINK } from "../../utils/key";

import { getError, setInputError } from "./errorAction";
import { clearNotification, customNotification } from "./notificationAction";
import isEmpty from "../../utils/isEmpty";

export const getOrderBySessionID = sID => async dispatch => {
	let result = "";

	await axios
		.get(`${XANO_APILINK}/session/${sID}/order`)
		.then(res => {
			result = res.data.order.ref;
		})
		.catch(err => {
			dispatch(getError(err));
		});

	return result;
};

export const getBookDetails = id => async dispatch => {
	let data = null;

	dispatch(setLoading(true));
	await axios
		.get(`${XANO_APILINK}/order/${id}`)
		.then(res => {
			data = res.data;
			dispatch(setLoading(false));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});

	return data;
};

export const createOrder = (data, sID) => dispatch => {
	dispatch(setLoading(true));
	dispatch(setInputError());
	dispatch(clearNotification());

	//console.log(data);
	axios
		.post(`${XANO_APILINK}/order?session_id=${sID}`, data)
		.then(async res => {
			let urlParams = new URLSearchParams(window.location.search);
			urlParams.set("order_ref", res.data.order.ref);
			const urlQueryString = urlParams.toString();
			window.history.replaceState(
				null,
				null,
				`${window.location.pathname}${!isEmpty(urlQueryString) ? `?${urlQueryString}` : ""}`
			);
			dispatch(customNotification("", "success", "createOrder"));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const setItemOfOrder = (data, oID) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNotification());

	axios
		.patch(`${XANO_APILINK}/order/${oID}/product`, data)
		.then(async res => {
			let urlParams = new URLSearchParams(window.location.search);
			urlParams.set("order_ref", oID);
			const urlQueryString = urlParams.toString();
			window.history.replaceState(
				null,
				null,
				`${window.location.pathname}${!isEmpty(urlQueryString) ? `?${urlQueryString}` : ""}`
			);
			dispatch(customNotification("", "success", "setItemOfOrder"));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const editOrderPromo = (data, oID) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNotification());
	dispatch(setInputError());
	axios
		.patch(`${XANO_APILINK}/order/${oID}/promo`, data)
		.then(async res => {
			dispatch(customNotification("", "success", "editOrderPromo"));
		})
		.catch(err => {
			dispatch(setLoading(false));
			if (err.response && !isEmpty(err.response.data)) {
				dispatch(setInputError([{ param: "promo_code", error: err.response.data.message }]));
			}

			dispatch(getError(err));
		});
};

export const editOrderNotes = (data, oID) => dispatch => {
	dispatch(clearNotification());
	axios
		.patch(`${XANO_APILINK}/order/${oID}/notes`, data)
		.then(async res => {
			dispatch(customNotification("", "success", "editOrderNotes"));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};
