import axios from "axios";
import { APILINK, XANO_APILINK } from "../../utils/key";
import { setLoading } from "./loadingAction";

import {
	GET_BUSINESS,
	GET_BUSINESS_LISTING,
	GET_BUSINESS_CATEGORY,
	GET_CUSTOM_FIELD,
	GET_BUSINESS_PROMO,
} from "./index";
import { getError } from "./errorAction";
import isEmpty from "../../utils/isEmpty";
import { getAllCatalog } from "./cartAction";
import { clearNotification, customNotification } from "./notificationAction";

//need to revise
export const getAllStoreWithListings = async () => {
	let data = [];
	await axios
		.get(`${XANO_APILINK}/sitemap`)
		.then(res => {
			data = res.data.sitemap;
		})
		.catch(err => {
			getError(err);
		});
	return data;
};

export const getBusinessIDbySubdomain = name => dispatch => {
	if (name) {
		dispatch(setLoading(true));
		axios
			.get(`${XANO_APILINK}/business?subdomain=${name}`)
			.then(res => {
				if (!isEmpty(res.data)) {
					//dispatch(getAllCatalog(res.data.business.business_id));
					dispatch({
						type: GET_BUSINESS,
						payload: res.data,
					});
				} else {
					dispatch({
						type: GET_BUSINESS,
						payload: {},
					});
				}
				dispatch(setLoading(false));
			})
			.catch(err => {
				dispatch({
					type: GET_BUSINESS,
					payload: {},
				});
				dispatch(setLoading(false));
				dispatch(getError(err, true));
			});
	} else {
		dispatch({
			type: GET_BUSINESS,
			payload: {},
		});
	}
};

export const getBusinessItem = (bID, sID) => dispatch => {
	dispatch(setLoading(true));
	axios
		.get(`${XANO_APILINK}/business/${bID}/product?session_id=${sID}`)
		.then(res => {
			const items = res.data.products;
			const active = [];
			const inactive = [];
			const arr = [];

			items.map(f => {
				if (f.total_stock <= 0) {
					inactive.push(f);
				} else {
					active.push(f);
				}
			});
			active.map((a, i) => {
				arr.push({ ...a, stockAvailable: true });
			});
			inactive.map((a, i) => {
				arr.push({ ...a, stockAvailable: false });
			});

			dispatch({
				type: GET_BUSINESS_LISTING,
				items: arr,
			});
			dispatch(setLoading(false));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getBusinessCategoriesWithProduct = bID => dispatch => {
	axios
		.get(`${XANO_APILINK}/business/${bID}/category`)
		.then(res => {
			const categories = res.data.categories;
			const arr = [];

			categories.map(c => {
				const { products, ...rest } = c;
				let category = { ...rest };
				let arrP = [];

				const items = c.products;
				const active = [];
				const inactive = [];

				items.map(f => {
					if (f.total_stock <= 0) {
						inactive.push(f);
					} else {
						active.push(f);
					}
				});

				active.map((a, i) => {
					arrP.push({ ...a, stockAvailable: true });
				});
				inactive.map((a, i) => {
					arrP.push({ ...a, stockAvailable: false });
				});

				category = { ...category, listings: arrP };
				arr.push(category);
			});

			dispatch({
				type: GET_BUSINESS_CATEGORY,
				payload: arr,
			});
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getBusinessPromo =
	(bID, promo, product_total = 0) =>
	dispatch => {
		dispatch(clearNotification());

		if (isEmpty(promo)) {
			dispatch({
				type: GET_BUSINESS_PROMO,
				payload: {},
			});
		} else {
			axios
				.post(`${XANO_APILINK}/business/${bID}/promo/${promo}`, {
					product_subtotal: product_total,
				})
				.then(res => {
					dispatch(customNotification(res.data.message, "success", "getBusinessPromo"));

					dispatch({
						type: GET_BUSINESS_PROMO,
						payload: res.data.promo,
					});
				})
				.catch(err => {
					dispatch(customNotification(err.response.data.message, "error"));
					dispatch({
						type: GET_BUSINESS_PROMO,
						payload: {},
					});
					dispatch(setLoading(false));
					dispatch(getError(err));
				});
		}
	};
