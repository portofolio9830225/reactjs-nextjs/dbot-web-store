import Axios from "axios";
import { GET_ERROR, SET_INPUT_ERROR } from "./index";
import { customNotification } from "./notificationAction";

export const getError =
  (err, noShowNoti = false) =>
  (dispatch) => {
    dispatch({
      type: GET_ERROR,
      payload: err,
    });

    if (!err.response) {
      if (!noShowNoti) {
        dispatch(customNotification("Network not detected", "error"));
      }
      console.log(err);
    } else {
      if (!noShowNoti) {
        dispatch(customNotification(err.response.data.message, "error"));
      }
      if (err.response.data && err.response.data.errors) {
        dispatch(setInputError(err.response.data.errors));
      }

      console.log(err.response.data);
    }
  };

export const setInputError =
  (err = []) =>
  (dispatch) => {
    dispatch({
      type: SET_INPUT_ERROR,
      payload: err,
    });
  };
