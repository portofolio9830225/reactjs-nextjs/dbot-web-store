import axios from "axios";
import isEmpty from "../../utils/isEmpty";
import { APILINK, XANO_APILINK } from "../../utils/key";
import { setLoading } from "./loadingAction";
import { getError } from "./errorAction";

import {
	GET_ITEM,
	CLEAR_ITEM_STATE,
	GET_ITEM_NA,
	GET_CART_ITEMS,
	DELETE_CART,
	CLEAR_CART,
	SET_CATEGORY,
} from "./index";
import { clearNotification, customNotification } from "./notificationAction";

export const clearItem = () => dispatch => {
	dispatch({
		type: CLEAR_ITEM_STATE,
	});
};

export const getItemDetails = (itmid, bID) => dispatch => {
	dispatch(setLoading(true));
	axios
		.get(
			`${XANO_APILINK}/business/${bID}/product/${itmid}?session_id=${sessionStorage.getItem("storeup:sessionID")}`
		)
		.then(res => {
			dispatch(clearItem());
			let { variants, images, ...details } = res.data.product;
			dispatch({
				type: GET_ITEM,
				payload: { details, variants, images },
			});
			dispatch(setLoading(false));
		})
		.catch(err => {
			dispatch({
				type: GET_ITEM,
				payload: {},
			});
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};
