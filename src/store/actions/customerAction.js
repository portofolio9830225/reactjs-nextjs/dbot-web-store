import axios from "axios";
import isEmpty from "../../utils/isEmpty";
import { APILINK, XANO_APILINK } from "../../utils/key";
import { SET_CUSTOMER } from "./";
import { getError, setInputError } from "./errorAction";
import { setLoading } from "./loadingAction";
import { clearNotification, customNotification } from "./notificationAction";

export const createSession =
	(id = null) =>
	dispatch => {
		let sID = id || sessionStorage.getItem("storeup:sessionID");

		if (!isEmpty(eval(sID))) {
			dispatch(getSessionInfo(sID));
		} else {
			axios
				.post(`${XANO_APILINK}/session`)
				.then(res => {
					dispatch(getSessionInfo(res.data.session.id, true));
				})
				.catch(err => {
					dispatch(getError(err));
				});
		}
	};

export const updateSessionInfo = (sID, data) => dispatch => {
	axios
		.patch(`${XANO_APILINK}/session/${sID}`, data)
		.then(res => {
			dispatch(customNotification("Customer Info validated", "success", "sessionInfo", false));
			dispatch(getSessionInfo(sID));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getSessionInfo = (sID, isInitial) => dispatch => {
	axios
		.get(`${XANO_APILINK}/session/${sID}`)
		.then(res => {
			console.log(res.data);
			sessionStorage.setItem("storeup:sessionID", res.data.session.id);

			dispatch({
				type: SET_CUSTOMER,
				payload: res.data.session,
				start: true,
			});
		})
		.catch(err => {
			dispatch(getError(err, true));
		});
};

export const endSession = () => dispatch => {
	sessionStorage.removeItem("storeup:sessionID");
	sessionStorage.removeItem("storeup:cart");
	dispatch({
		type: SET_CUSTOMER,
		payload: {},
		start: false,
	});
};
