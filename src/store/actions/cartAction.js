import axios from "axios";
import { APILINK } from "../../utils/key";

import { GET_ALL_CATALOG, GET_CART_BREAKDOWN, SET_CART_ITEM, GET_CATALOG_SHIPPING, GET_PAYMENT_METHOD } from "./index";
import { getError } from "./errorAction";
import isEmpty from "../../utils/isEmpty";
import { clearNotification } from "./notificationAction";
import { setLoading } from "./loadingAction";

export const getAllCatalog = bID => dispatch => {
	axios
		.get(`${APILINK}/v1/catalog/business/${bID}`)
		.then(res => {
			dispatch({
				type: GET_ALL_CATALOG,
				payload: [],
			});
			dispatch({
				type: GET_ALL_CATALOG,
				payload: res.data.catalogs,
			});
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const setCartItem =
	(cart, bID, data = [], replaceQuantity = false) =>
	dispatch => {
		let arr = [...cart];

		if (!isEmpty(data)) {
			data.map(itm => {
				let foundIndex = arr.findIndex(
					e => e.item_id === itm.item_id && e.variant_number === itm.variant_number
				);
				if (itm.quantity > 0) {
					if (foundIndex !== -1) {
						if (replaceQuantity) {
							arr[foundIndex].quantity = itm.quantity;
						} else {
							arr[foundIndex].quantity += itm.quantity;
						}
					} else {
						arr.push(itm);
					}
				} else {
					if (foundIndex !== -1) {
						arr.splice(foundIndex, 1);
					}
				}
			});
		}

		// if (!isEmpty(arr)) {
		// 	dispatch(getCartBreakdown({ catalog_id: catID, items: arr, business_id: bID }));
		// } else {
		// 	dispatch(getCartBreakdown());
		// }
		dispatch({
			type: SET_CART_ITEM,
			payload: [],
		});
		dispatch({
			type: SET_CART_ITEM,
			payload: arr,
		});
	};
