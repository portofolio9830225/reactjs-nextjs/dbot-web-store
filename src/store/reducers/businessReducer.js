import {
	GET_BUSINESS,
	GET_BUSINESS_CATEGORY,
	GET_BUSINESS_LISTING,
	GET_CUSTOM_FIELD,
	GET_BUSINESS_PROMO,
} from "../actions";

const initialState = {
	message: "",
	detail: {},
	listings: [],
	// activeListings: [],
	// inactiveListings: [],
	categories: [],
	customField: [],
	isFetched: false,
	isListingFetched: false,
	isCategoryFetched: false,
	promo: {},
};

export default function (state = initialState, action) {
	switch (action.type) {
		case GET_BUSINESS:
			return {
				...state,
				detail: action.payload,
				isFetched: true,
			};
		case GET_BUSINESS_LISTING:
			return {
				...state,
				listings: action.items,
				// activeListings: action.active,
				// inactiveListings: action.inactive,
				isListingFetched: true,
			};
		case GET_BUSINESS_CATEGORY:
			return {
				...state,
				categories: action.payload,
				isCategoryFetched: true,
			};
		case GET_CUSTOM_FIELD:
			return {
				...state,
				customField: action.payload,
			};
		case GET_BUSINESS_PROMO:
			return {
				...state,
				promo: action.payload,
			};
		default:
			return state;
	}
}
