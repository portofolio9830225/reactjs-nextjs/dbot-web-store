import isEmpty from "../../utils/isEmpty";
import { SET_CUSTOMER } from "../actions";

const initialState = {
	sessionID: null,
	name: null,
	phone: null,
	isFetched: false,
};

export default function (state = initialState, action) {
	switch (action.type) {
		case SET_CUSTOMER:
			if (action.start) {
				if (isEmpty(action.payload)) {
					return {
						...state,
						sessionID: null,
						name: null,
						phone: null,
						isFetched: true,
					};
				} else {
					return {
						...state,
						sessionID: action.payload.id,
						name: action.payload.name,
						phone: action.payload.phone,
						isFetched: true,
					};
				}
			}

		default:
			return state;
	}
}
