import { SET_PRODUCT_BOX_TYPE } from "../actions";

const initialState = {
	productBoxType: null,
};

export default function (state = initialState, action) {
	switch (action.type) {
		case SET_PRODUCT_BOX_TYPE: {
			console.log("Set:", action.payload);
			return {
				...state,
				productBoxType: action.payload,
			};
		}

		default:
			return state;
	}
}
