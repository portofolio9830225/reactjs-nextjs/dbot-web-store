import isEmpty from "../../utils/isEmpty";
import {
	GET_ITEM,
	CLEAR_ITEM_STATE,
	GET_CART_ITEMS,
	GET_ITEM_NA,
	GET_BOOKING_ID,
	DELETE_CART,
	CLEAR_CART,
	SET_CATEGORY,
} from "../actions";

const initialState = {
	itemPage: {
		details: {},
		images: [],
		pricing: [],
		shipping: [],
		variants: [],
		stock: 0,
	},
	isItemFetched: false,
	dateNA: [],
	cart: [],
	cartCustomField: [],
	cartConflictItem: [],
	isCartFetched: false,
	bookingID: "",
	url: "",
	prevCat: null,
};

export default function (state = initialState, action) {
	switch (action.type) {
		case CLEAR_ITEM_STATE:
			return {
				...state,
				itemPage: {
					details: {},
					images: [],
					pricing: [],
					shipping: [],
					variants: [],
					stock: 0,
				},
				isItemFetched: false,
				dateNA: [],
				bookingID: "",
				url: "",
			};

		case GET_ITEM:
			return {
				...state,
				itemPage: !isEmpty(action.payload)
					? {
							...state.itemPage,
							details: action.payload.details,
							images: action.payload.images,
							//pricing: action.payload.pricing,
							//shipping: action.payload.shippings,
							variants: action.payload.variants,
							//stock: action.payload.stocks,
					  }
					: { ...state.itemPage },
				isItemFetched: true,
			};
		case GET_CART_ITEMS: {
			let setCart = state.cart;

			if (!isEmpty(action.payload)) {
				let found = setCart.findIndex(e => e.cart_id === action.payload.cart_id);
				if (found !== -1) {
					setCart[found] = action.payload;
				} else {
					setCart.push(action.payload);
				}

				let list = [];
				setCart.map(e => {
					list.push(e.cart_id);
				});
				let bizCart = {
					business_id: setCart[0].business_id,
					list,
				};

				let sessionCart = sessionStorage.getItem("storeup:cart");

				if (!isEmpty(sessionCart)) {
					sessionCart = JSON.parse(sessionCart).cart;

					let bizIndex = sessionCart.findIndex(e => e.business_id === bizCart.business_id);
					if (bizIndex !== -1) {
						sessionCart[bizIndex] = bizCart;
					} else {
						sessionCart.push(bizCart);
					}
				} else {
					sessionCart = [];
					sessionCart.push(bizCart);
				}

				sessionStorage.setItem("storeup:cart", JSON.stringify({ cart: sessionCart }));
			} else {
				if (!isEmpty(setCart)) {
					let bizCart = {
						business_id: setCart[0].business_id,
						list: [],
					};

					let sessionCart = sessionStorage.getItem("storeup:cart");

					if (!isEmpty(sessionCart)) {
						sessionCart = JSON.parse(sessionCart).cart;

						let bizIndex = sessionCart.findIndex(e => e.business_id === bizCart.business_id);
						if (bizIndex !== -1) {
							sessionCart[bizIndex] = bizCart;
						} else {
							sessionCart.push(bizCart);
						}
					} else {
						sessionCart = [];
						sessionCart.push(bizCart);
					}

					sessionStorage.setItem("storeup:cart", JSON.stringify({ cart: sessionCart }));
				}
				setCart = [];
			}

			return {
				...state,
				cart: setCart,
				cartCustomField: action.customField,
				cartConflictItem: action.conflictItem,
				isCartFetched: true,
			};
		}
		case CLEAR_CART: {
			return {
				...state,
				cart: [],
			};
		}
		case DELETE_CART:
			return {
				...state,
				cart: state.cart.filter(e => e.cart_id !== action.payload),
			};
		case GET_ITEM_NA:
			return {
				...state,
				dateNA: action.payload.result,
			};

		case SET_CATEGORY:
			return {
				...state,
				prevCat: action.payload,
			};
		default:
			return state;
	}
}
