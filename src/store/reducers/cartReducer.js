import {
	GET_ALL_CATALOG,
	GET_PAYMENT_METHOD,
	GET_BOOKING_ID,
	GET_CART_BREAKDOWN,
	GET_CATALOG_SHIPPING,
	GET_WHATSAPP_DETAILS,
	SET_CART_ITEM,
} from "../actions";

const initialState = {
	message: "",
	catalog: [],
	list: [],
	breakdown: {},
	shipping: {},
	isFetched: false,
	billURL: "",
	whatsapp: {},
	paymentMethod: [],
};

export default function (state = initialState, action) {
	switch (action.type) {
		case SET_CART_ITEM: {
			sessionStorage.setItem("storeup:cart", JSON.stringify(action.payload));
			return {
				...state,
				list: action.payload,
				isFetched: true,
			};
		}
		case GET_ALL_CATALOG: {
			return {
				...state,
				catalog: action.payload,
			};
		}
		case GET_CART_BREAKDOWN: {
			return {
				...state,
				breakdown: action.payload,
			};
		}
		case GET_CATALOG_SHIPPING: {
			return {
				...state,
				shipping: action.payload,
			};
		}
		case GET_PAYMENT_METHOD:
			return {
				...state,
				paymentMethod: action.payload,
			};
		case GET_BOOKING_ID:
			return {
				...state,
				billUrl: action.payload,
			};
		case GET_WHATSAPP_DETAILS: {
			return {
				...state,
				whatsapp: {
					order_id: action.payload,
					phone: action.phone,
				},
			};
		}
		default:
			return state;
	}
}
