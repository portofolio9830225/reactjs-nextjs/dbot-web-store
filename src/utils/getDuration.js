const day_ms = 1000 * 60 * 60 * 24;
const getDuration = (start_date, end_date) => {
  const start = new Date(start_date);
  const end = new Date(end_date);
  const duration = (end.getTime() - start.getTime()) / day_ms;
  return duration;
};

export default getDuration;
