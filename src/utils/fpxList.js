const Alliance = "/images/bank/AllianceBank.jpg";
const AffinBank = "/images/bank/affinBank.png";
const Ambank = "/images/bank/ambank.png";
const CIMB = "/images/bank/cimbclicks.png";
const BankIslam = "/images/bank/bankIslam.jpg";
const BankRakyat = "/images/bank/bankrakyat.png";
const BankMuamalat = "/images/bank/bankMuamalat.png";
const BSN = "/images/bank/bsn.jpg";
const CitiBank = "/images/bank/citibank.jpeg";
const HongLeong = "/images/bank/hongleongBank.png";
const HSBC = "/images/bank/hsbc.jpg";
const Kuwait = "/images/bank/kuwaitFH.jpg";
const Maybank = "/images/bank/maybank.png";
const OCBC = "/images/bank/ocbc.png";
const PublicBank = "/images/bank/publicBank.png";
const RHB = "/images/bank/rhb.jpg";
const StandardCharted = "/images/bank/standardcharted.png";
const UOB = "/images/bank/uob.jpg";

const fpxList = [
  {
    code: "ABMB0212",
    name: "Alliance Bank",
    status: false,
    image: Alliance,
  },
  {
    code: "ABB0233",
    name: "Affin Bank",
    status: false,
    image: AffinBank,
  },
  {
    code: "AMBB0209",
    name: "AmBank",
    status: false,
    image: Ambank,
  },
  {
    code: "BCBB0235",
    name: "CIMB Clicks",
    status: false,
    image: CIMB,
  },
  {
    code: "BIMB0340",
    name: "Bank Islam",
    status: false,
    image: BankIslam,
  },
  {
    code: "BKRM0602",
    name: "Bank Rakyat",
    status: false,
    image: BankRakyat,
  },
  {
    code: "BMMB0341",
    name: "Bank Muamalat",
    status: false,
    image: BankMuamalat,
  },
  {
    code: "BSN0601",
    name: "BSN",
    status: false,
    image: BSN,
  },
  {
    code: "CIT0217",
    name: "Citibank Berhad",
    status: false,
    image: CitiBank,
  },
  {
    code: "HLB0224",
    name: "Hong Leong Bank",
    status: false,
    image: HongLeong,
  },
  {
    code: "HSBC0223",
    name: "HSBC Bank",
    status: false,
    image: HSBC,
  },
  {
    code: "KFH0346",
    name: "Kuwait Finance House",
    status: false,
    image: Kuwait,
  },
  {
    code: "MB2U0227",
    name: "Maybank2u",
    status: false,
    image: Maybank,
  },
  {
    code: "MBB0227",
    name: "Maybank2E",
    status: false,
    image: Maybank,
  },
  {
    code: "MBB0228",
    name: "Maybank2E",
    status: false,
    image: Maybank,
  },
  {
    code: "OCBC0229",
    name: "OCBC Bank",
    status: false,
    image: OCBC,
  },
  {
    code: "PBB0233",
    name: "Public Bank",
    status: false,
    image: PublicBank,
  },
  {
    code: "RHB0218",
    name: "RHB Now",
    status: false,
    image: RHB,
  },
  {
    code: "SCB0216",
    name: "Standard Chartered",
    status: false,
    image: StandardCharted,
  },
  {
    code: "UOB0226",
    name: "UOB Bank",
    status: false,
    image: UOB,
  },
];

export default fpxList;
