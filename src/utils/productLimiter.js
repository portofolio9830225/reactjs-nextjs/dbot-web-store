import isEmpty from "./isEmpty";

const productLimiter = (minimum = null, maximum = null, stock, cart = 0) => {
	let min = 1;
	let max = stock;

	if (!isEmpty(minimum)) {
		let newMin = minimum - cart;
		min = newMin < 0 ? 0 : newMin;
	}
	if (min == 0 || cart >= minimum) {
		min = 1;
	}

	if (!isEmpty(maximum) && maximum < stock) {
		max = maximum;
	}
	max -= cart;
	if (max <= 0) {
		max = 0;
	}

	return { min, max };
};

export default productLimiter;
