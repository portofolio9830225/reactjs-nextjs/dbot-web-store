export const mainBgColor = "#fff";
// export const mainBgColor = "#F5F5F7";

export const primaryLight = "#6bff96";
export const primary = "#25d366";
export const primaryDark = "#00a038";

export const secondaryDark = "#003EBD";
export const secondary = "#0D67F1";
export const secondaryLight = "#6994FF";

export const greyblack = "#8E8E93"; // SystemGrey
export const greydark = "#AEAEB2"; // SystemGrey2
export const grey = "#C7C7CC"; // SystemGrey3
export const greylight = "#D1D1D6"; // SystemGrey4
export const greywhite = "#E5E5EA"; // SystemGrey5
export const black = "#252525";

export const successColor = "#00a038";
export const errorColor = "rgb(255,59,48)";
