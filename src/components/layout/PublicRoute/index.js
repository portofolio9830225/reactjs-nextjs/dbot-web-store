import React from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { withStyles } from "@material-ui/core/styles";

import { Typography, Slide, withWidth, Dialog } from "@material-ui/core/";

import isEmpty from "../../../utils/isEmpty";

import { clearNotification } from "../../../store/actions/notificationAction";
import NavBar from "../../mini/NavBar";
import { mainBgColor, black } from "../../../utils/ColorPicker";
import Loading from "../../mini/Loading";
import {
	getBusinessItem,
	getBusinessIDbySubdomain,
	getBusinessCategoriesWithProduct,
} from "../../../store/actions/businessAction";
import PageHelmet from "../../mini/PageHelmet";
import NotFound from "../../../components/NotFound";
import { nvhDefault, nvhMobile } from "../../../utils/navbarHeight";
import { createSession, endSession } from "../../../store/actions/customerAction";
import { setCartItem } from "../../../store/actions/cartAction";
import StoreUpTag from "../../mini/StoreUpTag";
import { setLoading } from "../../../store/actions/loadingAction";
import store from "../../../store";
import { getBookDetails } from "../../../store/actions/bookingAction";

const styles = theme => ({
	root: {
		flexGrow: 1,
		zIndex: 1,
		//overflow: "hidden",
		position: "relative",
		display: "flex",
		flexDirection: "column",
		width: "100%",
		backgroundColor: mainBgColor,
	},

	snackbarContainer: {
		width: "100%",
		height: "35px",
		position: "fixed",
		bottom: 0,
		zIndex: 1000,
	},
	errorSnackbar: {
		height: "100%",
		backgroundColor: "firebrick",
		width: "100%",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
	},
	successSnackbar: {
		height: "100%",
		backgroundColor: "#00A86B",
		width: "100%",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
	},
	snackbarMsg: {
		color: "white",
		fontSize: "1.3rem",
	},
	content: {
		flexGrow: 1,
		display: "flex",
		flexDirection: "column",
		width: "100%",
	},
	main: {
		display: "flex",
		flexDirection: "column",
		alignSelf: "center",
		minHeight: `calc(100vh - ${nvhDefault})`,
		width: "100%",
		maxWidth: "600px",
		[theme.breakpoints.down("xs")]: {
			minHeight: `calc(100vh - ${nvhMobile})`,
		},
	},
	loadingBox: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		alignSelf: "center",
		height: "100vh",
		paddingBottom: "5rem",
		boxSizing: "border-box",
	},
	loadingBoxIcon: {
		width: "150px",
		height: "150px",
		marginBottom: "2rem",
	},
	DialogRoot: {
		backgroundColor: "rgba(0,0,0,0)",
		boxShadow: "none",
	},
});

class PublicComponent extends React.Component {
	state = {
		mobileOpen: false,
		errorSnackbar: false,
		successSnackbar: false,
		message: "",
		cartStatus: false,
		isReady: false,
		debug: 0,
		fetchedOrder: null,
	};

	numberWheelScroll = e => {
		if (document.activeElement.type === "number") {
			document.activeElement.blur();
		}
	};

	async componentDidMount() {
		window.onpageshow = function (event) {
			if (event.persisted) {
				store.dispatch(setLoading(true));
				setTimeout(() => {
					window.location.reload();
				}, 800);
			}
		};
		document.addEventListener("wheel", this.numberWheelScroll);

		this.props.getBusinessIDbySubdomain(this.props.pageProps.bizID);
	}

	componentWillReceiveProps(nextProps) {
		if (!isEmpty(nextProps.noti.message) && this.props.noti.message === "") {
			if (nextProps.noti.status === "error" && this.props.noti.status === "") {
				this.setState({
					errorSnackbar: true,
				});
			} else if (nextProps.noti.status === "success" && this.props.noti.status === "") {
				this.setState({
					successSnackbar: true,
				});
			}
			this.setState({
				message: nextProps.noti.message,
			});
			this.closeSnackbar();
			this.clearNoti();
		}
	}

	async componentDidUpdate(prevProps, prevState) {
		if (!prevProps.business.isFetched && this.props.business.isFetched) {
			if (isEmpty(this.props.business.detail)) {
				this.appReady();
			} else {
				if (window.location.pathname.split("/")[1] !== "o" && window.location.pathname.split("/")[1] !== "s") {
					this.setState({
						debug: 1,
					});
					let urlParams = new URLSearchParams(window.location.search);
					let oID = urlParams.get("order_ref");

					if (window.location.pathname.split("/")[1] === "bag" && !isEmpty(oID)) {
						const { order } = await this.props.getBookDetails(oID);
						this.setState(
							{
								fetchedOrder: order,
							},
							() => {
								this.props.createSession(this.state.fetchedOrder.session_id);
							}
						);
					} else {
						this.props.createSession();
					}

					this.setState({
						debug: 2,
					});
				} else {
					this.props.endSession();
					this.appReady();
				}
			}
		}
		if (!prevProps.customer.isFetched && this.props.customer.isFetched) {
			this.setState({
				debug: 3,
			});
			this.props.getBusinessItem(this.props.business.detail.id, this.props.customer.sessionID);
			this.setState({
				debug: 4,
			});
		}

		if (!prevProps.business.isListingFetched && this.props.business.isListingFetched) {
			this.props.getBusinessCategoriesWithProduct(this.props.business.detail.id);
			this.setState({
				debug: 5,
			});
		}
		if (!prevProps.business.isCategoryFetched && this.props.business.isCategoryFetched) {
			this.setState({
				debug: 6,
			});
			let cartStorage = [];
			let fetchedCartStorage = sessionStorage.getItem("storeup:cart");

			if (
				!isEmpty(this.state.fetchedOrder) &&
				this.props.business.detail.id == this.state.fetchedOrder.business.id &&
				isEmpty(fetchedCartStorage)
			) {
				let { products } = this.state.fetchedOrder;
				products.map(p => {
					let productCart = {
						details: p,
						image: p.image,
						item_id: p.product_ref,
						quantity: p.quantity,
						variant_number: p.variant_id,
						variant_name: p.variant_name,
						available: false,
					};

					if (
						!isEmpty(
							this.props.business.listings.filter(fil => fil.active).find(f => f.ref === p.product_ref)
						)
					) {
						productCart.available = true;
					}
					cartStorage.push(productCart);
				});
			} else {
				//clear orderFetched with its url params START
				this.setState({
					fetchedOrder: null,
				});
				let url = new URL(window.location.href);
				let urlParams = new URLSearchParams(url.search);
				if (urlParams.has("order_ref")) {
					urlParams.delete("order_ref");
					let urlQueryString = urlParams.toString();

					window.history.replaceState(
						null,
						null,
						`${window.location.pathname}${!isEmpty(urlQueryString) ? `?${urlQueryString}` : ""}`
					);
				}
				//clear orderFetched with its url params END

				if (!isEmpty(fetchedCartStorage)) {
					let parsed = JSON.parse(fetchedCartStorage);
					parsed.map(e => {
						let productCart = { ...e, available: false };

						if (
							!isEmpty(
								this.props.business.listings.filter(fil => fil.active).find(f => f.ref === e.item_id)
							)
						) {
							productCart.available = true;
						}
						cartStorage.push(productCart);
					});
				}
			}

			this.setState({
				debug: 7,
			});
			this.props.setCartItem(cartStorage, this.props.business.detail.business_id);
			this.setState({
				debug: 8,
			});
		}
		if (!prevProps.cart.isFetched && this.props.cart.isFetched) {
			this.appReady();
		}
	}

	componentWillUnmount() {
		document.removeEventListener("wheel", this.numberWheelScroll);
	}

	appReady = () => {
		this.setState({
			isReady: true,
		});
	};

	closeSnackbar = () => {
		setTimeout(() => {
			this.setState({
				errorSnackbar: false,
				successSnackbar: false,
			});
		}, 2500);
	};

	clearNoti = () => {
		setTimeout(() => {
			this.props.clearNotification();
		}, 3000);
	};

	handleDrawerToggle = () => {
		this.setState({ mobileOpen: !this.state.mobileOpen });
	};

	handleCart = state => {
		this.setState({
			cartStatus: state,
		});
	};

	render() {
		const { width, classes, theme, secondaryNavbar } = this.props;
		const { errorSnackbar, successSnackbar, message, cartStatus } = this.state;
		const { status } = this.props.noti;
		const { detail } = this.props.business;
		const isLoading = this.props.loading.status;
		const hideCart = this.props.router.asPath.includes("checkout");
		const hideMenu = true;

		if (this.state.isReady) {
			if (!isEmpty(detail)) {
				return (
					<div className={classes.root}>
						<PageHelmet
							metadata={{
								title: `${!isEmpty(detail) ? `${detail.name} | StoreUp` : "StoreUp"}`,
								description:
									"We are incredibly proud to partner with amazing brands that are committed in making a meaningful impact to the communities they connect with.",
								keywords: `${detail.name} store, ${detail.name} store storeup, ${detail.name} storeup.io store, ${detail.name} store storeup.io, ${detail.name} storeup store,storeup store, storeup store directory, storeup directory, storeup.io store, storeup.io store directory, storeup.io directory`,
								image: !isEmpty(detail) ? detail.logo : "/images/noLogo.png",
							}}
						>
							<link rel="shortcut icon" href={detail.logo} />
						</PageHelmet>

						{/* {isLoading && (
              <Dialog
                open={isLoading}
                elevation={0}
                classes={{
                  paper: classes.DialogRoot,
                }}
              >
                <Loading open={isLoading} />
              </Dialog>
            )} */}

						{!isEmpty(this.props.noti.message) && this.props.noti.show && (
							<div className={classes.snackbarContainer}>
								{status === "error" && (
									<Slide in={errorSnackbar} direction="up">
										<div className={classes.errorSnackbar}>
											<Typography className={classes.snackbarMsg}>{message}</Typography>
										</div>
									</Slide>
								)}
								{status === "success" && (
									<Slide in={successSnackbar} direction="up">
										<div className={classes.successSnackbar}>
											<Typography className={classes.snackbarMsg}>{message}</Typography>
										</div>
									</Slide>
								)}
							</div>
						)}
						{!isLoading && (
							<NavBar
								isShowMenu={!hideMenu}
								isShowCart={!hideCart && !hideMenu}
								onChangeCart={this.handleCart}
								navs={
									hideMenu
										? []
										: [
												{
													title: "Download app",
													action_link: "https://app.storeup.io/downloads",
												},
										  ]
								}
							/>
						)}

						<div className={classes.content}>
							{isLoading && (
								<div className={classes.loadingBox}>
									{/* <img
                    src="/images/logo.png"
                    className={classes.loadingBoxIcon}
                  /> */}
									<Loading open={isLoading} />
								</div>
							)}
							<div className={classes.main} style={isLoading ? { display: "none" } : {}}>
								{this.props.children}
							</div>
						</div>
					</div>
				);
			} else {
				return <NotFound business />;
			}
		} else {
			return <div>{/* <Typography>{this.state.debug}</Typography> */}</div>;
		}
	}
}

const mapStateToProps = state => ({
	item: state.item,
	business: state.business,
	customer: state.customer,
	cart: state.cart,
	noti: state.notification,
	loading: state.loading,
});

export default connect(mapStateToProps, {
	setLoading,
	clearNotification,
	getBusinessIDbySubdomain,
	createSession,
	getBusinessItem,
	getBusinessCategoriesWithProduct,
	setCartItem,
	endSession,
	getBookDetails,
})(withStyles(styles, { withTheme: true })(withRouter(withWidth()(PublicComponent))));
