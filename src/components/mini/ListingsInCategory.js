import { Typography, withStyles } from "@material-ui/core";
import { withRouter } from "next/router";
import React, { Component } from "react";
import { connect } from "react-redux";
import { clearItem } from "../../store/actions/itemAction";
import { setLoading } from "../../store/actions/loadingAction";
import { greywhite, secondary } from "../../utils/ColorPicker";
import isEmpty from "../../utils/isEmpty";
import ProductBox from "./ProductBox1";

class ListingsInCategory extends Component {
	handlePage = link => () => {
		if (link.includes("http")) {
			window.open(link);
		} else {
			if (this.props.router.asPath !== link) {
				this.props.setLoading(true);
			}

			this.props.router.push(link, undefined, { shallow: true });
		}
	};

	handleItem =
		(id = null) =>
		() => {
			if (!isEmpty(id)) {
				let link = `/p/${id}`;

				//this.props.getItemDetails(id, this.props.business.detail.business_id);
				this.handlePage(link)();
			} else {
				this.props.clearItem();
			}
		};

	render() {
		const { className, productBoxType, name, onPress, listings } = this.props;
		const isProductList = productBoxType === 1;

		let products = listings.slice(0, isProductList ? 3 : 4);

		return (
			<div
				style={{
					display: "flex",
					flexDirection: "column",
					width: "100%",
					borderBottom: `1px solid ${greywhite}`,
					padding: "8px 0",
				}}
			>
				<div
					style={{
						display: "flex",
						flexDirection: "row",
						alignItems: "center",
						justifyContent: "space-between",
						height: "40px",
					}}
				>
					<Typography style={{ fontWeight: 600, fontSize: "20px" }}>{name}</Typography>
					<Typography
						style={{
							padding: "12px",
							paddingRight: 0,
							color: secondary,
							cursor: "pointer",
							fontSize: "16px",
						}}
						onClick={onPress}
					>
						See all
					</Typography>
				</div>
				<div className={className}>
					{products.map((p, i) => {
						return (
							<ProductBox
								key={i}
								inactive={!p.stockAvailable}
								isFirst={i == 0}
								title={p.name}
								desc={p.description}
								img={!isEmpty(p.images) ? p.images[0] : null}
								price={p.price}
								another_price={p.another_price}
								stock={p.total_stock}
								max={p.max_limit}
								min={p.min_limit}
								sold={p.number_sold}
								itmID={p.ref}
								onClick={this.handleItem(p.ref, p.name)}
								type={productBoxType}
							/>
						);
					})}
				</div>
			</div>
		);
	}
}

export default connect(null, { clearItem, setLoading })(withRouter(ListingsInCategory));
