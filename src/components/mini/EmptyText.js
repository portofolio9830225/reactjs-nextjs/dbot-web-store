import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const styles = (theme) => ({
  emptyText: {
    color: "dimgrey",
    fontWeight: 300,
    fontSize: "1.4rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.1rem",
    },
  },
});

class BigTextSection extends Component {
  render() {
    const { classes, children } = this.props;

    return <Typography className={classes.emptyText}>{children}</Typography>;
  }
}
// export default withRouter(withStyles(styles)(BigTextSection));
export default withStyles(styles)(BigTextSection);
