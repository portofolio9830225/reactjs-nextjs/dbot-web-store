import React, { Component } from "react";
import { withRouter } from "next/router";

import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Collapse, Fade } from "@material-ui/core";
import { ChevronLeftRounded, ExpandLessRounded } from "@material-ui/icons";
import { greydark, secondary } from "../../utils/ColorPicker";

const styles = (theme) => ({
  root: {
    width: "100%",
    //padding: "1rem 2rem",
    padding: "1rem 0",
    boxSizing: "border-box",
    backgroundColor: "white",
  },
  headerContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  text: {
    fontSize: "1.2rem",
    fontWeight: 400,
  },
  collapseContainer: {
    width: "100%",
    boxSizing: "border-box",
    paddingTop: "1rem",
    paddingLeft: "0.8rem",
  },
});

// TYPE
// 1: page redirect
// 2: On drawer

class DrawerList extends Component {
  state = {
    isOpen: false,
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.isOpen !== prevState.isOpen) {
      if (this.props.onExpandChange) {
        this.props.onExpandChange(this.state.isOpen);
      }
    }
  }

  handleExpand = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  };

  render() {
    const { classes, text, last, collapsedChild, onClick, onExpandChange } =
      this.props;
    const { isOpen } = this.state;

    return (
      <div
        className={classes.root}
        style={{ borderBottom: last ? "none" : "1px solid gainsboro" }}
        onClick={onClick || this.handleExpand}
      >
        <div className={classes.headerContainer}>
          {/* {collapsedChild && (
            <div style={{ }}>
              <Fade in={isOpen}>
                <ChevronLeftRounded
                  style={{  marginTop: "0.2rem", color: greydark }}
                />
              </Fade>
            </div>
          )} */}
          <Typography className={classes.text}>{text}</Typography>
          {/* {collapsedChild && ( */}
          <ExpandLessRounded
            style={{
              transform: collapsedChild
                ? isOpen
                  ? "rotate(0deg)"
                  : "rotate(-180deg)"
                : "rotate(90deg)",
              transition: "all 0.3s",
              marginTop: "0.2rem",
              color: "#c4c4c7",
            }}
          />
          {/* )} */}
        </div>

        {collapsedChild && (
          <Collapse in={this.state.isOpen}>
            <div className={classes.collapseContainer}>{collapsedChild}</div>
          </Collapse>
        )}
      </div>
    );
  }
}

// export default withStyles(styles)(withRouter(Footer));
export default withStyles(styles)(withRouter(DrawerList));
