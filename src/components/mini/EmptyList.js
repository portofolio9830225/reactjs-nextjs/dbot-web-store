import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import React, { Component } from "react";
import { grey, greydark } from "../../utils/ColorPicker";

class EmptyList extends Component {
	render() {
		const { classes, title, desc } = this.props;
		return (
			<div className={classes.emptyList}>
				<Typography className={classes.emptyListTitle}>{title}</Typography>
				<Typography className={classes.emptyListText}>{desc}</Typography>
			</div>
		);
	}
}

const styles = theme => ({
	emptyList: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		marginTop: "5rem",
		width: "100%",
	},
	emptyListTitle: {
		fontWeight: 600,
		width: "80%",
		maxWidth: "600px",
		fontSize: "17px",
		textAlign: "center",
		color: grey,
	},
	emptyListText: {
		width: "80%",
		maxWidth: "600px",
		marginBottom: "2rem",
		fontSize: "12px",
		textAlign: "center",
		color: grey,
		// [theme.breakpoints.down("sm")]: {
		// 	marginBottom: "1rem",
		// },
	},
});

export default withStyles(styles)(EmptyList);
