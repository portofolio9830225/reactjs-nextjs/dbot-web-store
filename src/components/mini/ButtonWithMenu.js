import React, { Component } from "react";

import { withStyles } from "@material-ui/core/styles";
import { Menu, MenuItem } from "@material-ui/core";
import isEmpty from "../../utils/isEmpty";

const styles = (theme) => ({
  navButton: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "2rem",
    padding: "0.7rem 1rem",
    borderRadius: 10,
    transition: "all 0.3s",
    letterSpacing: "0.5px",
    fontSize: "1rem",
    color: "black",
    "&:hover": {
      backgroundColor: "gainsboro",
      cursor: "pointer",
    },
  },
  navButtonInv: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "2rem",
    padding: "0.7rem 1rem",
    borderRadius: 10,
    transition: "all 0.3s",
    letterSpacing: "0.5px",
    fontSize: "1rem",
    color: "white",
    "&:hover": {
      backgroundColor: "dimgrey",
      cursor: "pointer",
    },
  },
});

class ButtonWithMenu extends Component {
  state = {
    modal: null,
  };

  handlePage = (link) => () => {
    this.setState({
      modal: null,
    });
    window.location.href = link;
  };

  handleModal = (state) => (e) => {
    this.setState({
      modal: state ? e.currentTarget : null,
    });
  };

  render() {
    const { modal } = this.state;
    const { classes, children, inverted, list } = this.props;

    return (
      <div className={classes.root}>
        <div
          className={inverted ? classes.navButtonInv : classes.navButton}
          onClick={this.handleModal(true)}
        >
          {children}
        </div>
        {!isEmpty(list) ? (
          <Menu
            anchorEl={modal}
            open={!isEmpty(modal)}
            onClose={this.handleModal(false)}
          >
            {list.map((e, i) => (
              <MenuItem key={i} onClick={this.handlePage(e.action_link)}>
                {e.title}
              </MenuItem>
            ))}
          </Menu>
        ) : null}
      </div>
    );
  }
}
// export default withStyles(styles)(withRouter(ButtonWithMenu));
export default withStyles(styles)(ButtonWithMenu);
