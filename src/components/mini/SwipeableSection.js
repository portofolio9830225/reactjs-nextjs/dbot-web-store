import React, { Component } from "react";
import { withRouter } from "next/router";
import { withStyles } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";

import SectionColorBackground from "./SectionColorBackground";
import { greylight, secondaryDark } from "../../utils/ColorPicker";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);
// const AutoPlaySwipeableViews = SwipeableViews;

const styles = (theme) => ({
  root: {
    position: "relative",
    width: "100%",
    // padding: "4rem 1.5rem",
    display: "flex",
    boxSizing: "border-box",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    // [theme.breakpoints.down("xs")]: {
    //   padding: "2rem 1rem",
    // },
  },
  content: {
    width: "100%",
    maxWidth: "1300px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  slideContainer: {
    display: "flex",
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    // margin: "2rem auto",
    marginBottom: "1rem",
  },
  slideTrack: {
    width: "100%",
  },

  slideIndicatorContainer: {
    zIndex: 10,
    marginTop: "0.5rem",
    display: "flex",
    flexDirection: "row",
    [theme.breakpoints.down("xs")]: {
      marginTop: 0,
    },
  },
  slideIndicator: {
    transition: "all 0.5s",
    cursor: "pointer",
    margin: "0 0.3rem",
    width: "10px",
    height: "10px",
    borderRadius: "5px",
    [theme.breakpoints.down("xs")]: {
      width: "6px",
      height: "6px",
      borderRadius: "3px",
    },
  },
});

class SwipeableSection extends Component {
  state = {
    index: 0,
  };

  handleScrollView = (index) => {
    this.setState({
      index,
    });
  };

  handleIndex = (index) => () => {
    this.setState({
      index,
    });
  };

  render() {
    const { classes, color, gradient, slides } = this.props;
    const { index } = this.state;

    return (
      <div className={classes.root}>
        <SectionColorBackground
          color={color}
          gradient={gradient}
          horizontal="full"
          vertical="full"
        />
        <div className={classes.content}>
          <div className={classes.slideContainer}>
            <AutoPlaySwipeableViews
              className={classes.slideTrack}
              enableMouseEvents
              onChangeIndex={this.handleScrollView}
              index={index}
            >
              {slides}
            </AutoPlaySwipeableViews>
          </div>
          <div className={classes.slideIndicatorContainer}>
            {slides.length > 1 &&
              slides.map((e, i) => {
                return (
                  <div
                    onClick={this.handleIndex(i)}
                    style={{
                      backgroundColor: index === i ? secondaryDark : greylight,
                    }}
                    className={classes.slideIndicator}
                  />
                );
              })}
          </div>
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(withRouter(SwipeableSection));
