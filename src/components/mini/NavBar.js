import React, { Component } from "react";
import { withRouter } from "next/router";
import { Waypoint } from "react-waypoint";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { Typography, Slide, Paper, Button, withWidth, Drawer, InputBase } from "@material-ui/core";

import isEmpty from "../../utils/isEmpty";

import { mainBgColor, greydark, secondary } from "../../utils/ColorPicker";
import { isSafari } from "react-device-detect";
import { nvhDefault, nvhMobile } from "../../utils/navbarHeight";
import { MenuRounded, LocalMallOutlined } from "@material-ui/icons";
// import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";
import DrawerList from "./DrawerList";
import StoreUpTag from "./StoreUpTag";
import { customNotification } from "../../store/actions/notificationAction";
import { setLoading } from "../../store/actions/loadingAction";

const styles = theme => ({
	root: {
		display: "flex",
		flexDirection: "column",
		position: "relative",
		width: "100%",
	},
	waypointTriggerBox: {
		height: "47px",
		position: "absolute",
		zIndex: -1,
		width: "100%",
	},

	navBar: {
		width: "100%",
		position: "fixed",
		top: 0,
		zIndex: 1000,
		transition: "all 0.3s",
		display: "flex",
		flexDirection: "column",
		backgroundColor: mainBgColor,
	},
	clearContainer: {
		height: nvhDefault,
		zIndex: -1,
		width: "100%",
		[theme.breakpoints.down("xs")]: {
			height: nvhMobile,
		},
	},
	content: {
		height: nvhDefault,
		zIndex: 100,
		display: "flex",
		flexDirection: "row",
		// justifyContent: "space-between",
		justifyContent: "space-between",
		alignItems: "center",
		alignSelf: "center",
		width: "100%",
		maxWidth: "600px",
		boxSizing: "border-box",
		//padding: "0 1.5rem",
		backgroundColor: "transparent",
		[theme.breakpoints.down("xs")]: {
			height: nvhMobile,
			padding: "0 1rem",
		},
	},

	businessHeader: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		height: "100%",
		cursor: "pointer",
		[theme.breakpoints.down("xs")]: {
			flexDirection: "column",
			alignItems: "center",
			justifyContent: "center",
		},
	},
	businessLogo: {
		height: "85%",
		borderRadius: "7px",
		overflow: "hidden",
	},
	businessText: {
		fontSize: "1rem",
		marginTop: "0.1rem",
	},
	navContainer: {
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
	},
	navMenu: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "flex-end",
		alignItems: "center",
		padding: "0.7rem 0",
	},
	navButton: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		marginLeft: "2rem",
		borderRadius: 10,
		transition: "all 0.3s",
		letterSpacing: "0.5px",
		fontSize: "1rem",
		color: "black",
		"&:hover": {
			backgroundColor: "gainsboro",
			cursor: "pointer",
		},
	},
	navButtonInv: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		marginLeft: "2rem",
		borderRadius: 10,
		transition: "all 0.3s",
		letterSpacing: "0.5px",
		fontSize: "1rem",
		color: "white",
		"&:hover": {
			backgroundColor: "dimgrey",
			cursor: "pointer",
		},
	},
	langIcon: {
		fontSize: "1.5rem",
	},
	menuDrawer: {
		height: "100vh",
		backgroundColor: "rgba(0,0,0,1)",
		display: "flex",
		flexDirection: "column",
		padding: "1rem",
		boxSizing: "border-box",
	},
	closeIcon: {
		alignSelf: "flex-end",
		padding: "1rem",
		paddingRight: 0,
		//fontSize: "2.5rem",
		fontSize: "1.3rem",
		//textDecoration: "underline",
		cursor: "pointer",
		color: secondary,
	},
	navList: {
		display: "flex",
		flexDirection: "column",
	},
	navText: {
		color: "white",
		fontSize: "2.5rem",
		letterSpacing: "1px",
		fontWeight: "bold",
		padding: "0.7rem 0.5rem",
		cursor: "pointer",
	},
	cartDrawer: {
		height: "100vh",
		width: "80%",
		maxWidth: "900px",
		backgroundColor: "white",
		display: "flex",
		flexDirection: "column",
		//padding: "1rem 0 0",

		boxSizing: "border-box",
		[theme.breakpoints.down("xs")]: {
			width: "100%",
		},
	},
	bizDrawer: {
		height: "100vh",
		width: "80%",
		maxWidth: "380px",
		backgroundColor: "#f2f2f7",
		//backgroundColor: "red",
		display: "flex",
		flexDirection: "column",
		justifyContent: "space-between",
		padding: "1rem 0 0",
		boxSizing: "border-box",
		[theme.breakpoints.down("xs")]: {
			maxWidth: "none",
			width: "100%",
		},
	},
	businessDrawer: {
		width: "100%",
		height: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		//justifyContent: "space-between",
		marginBottom: 20,

		boxSizing: "border-box",
		[theme.breakpoints.down("xs")]: {
			height: isSafari ? "90%" : "100%",
		},
	},
	businessImgContainer: {
		// boxShadow: "2px 5px 8px darkgrey",
		boxShadow:
			"-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
		width: "90px",
		height: "90px",
		borderRadius: "50%",
		margin: "1rem",
		[theme.breakpoints.down("xs")]: {
			alignSelf: "center",
		},
	},
	businessImg: {
		maxWidth: "100%",
		maxHeight: "100%",
		borderRadius: "50%",
	},
	businessName: {
		fontSize: "1.8rem",
		fontWeight: 400,
		textAlign: "center",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.5rem",
		},
	},
	businessDesc: {
		color: greydark,
		fontSize: "1.1rem",
		[theme.breakpoints.down("xs")]: {
			// fontSize: "1rem",
		},
	},
	businessDrawerHeader: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		boxSizing: "border-box",
		padding: "0 2rem",
	},
	storeDetails: {
		margin: "1rem 0",
		display: "flex",
		flexDirection: "column",
		//alignItems: "center",
		cursor: "pointer",
		width: "100%",
		marginTop: "2.5rem",
	},
	contactDetails: {
		margin: "2rem 0 1rem",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		cursor: "pointer",
		width: "100%",
	},
	cityHoursBox: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center",

		[theme.breakpoints.down("xs")]: {
			flexDirection: "column",
			alignItems: "flex-start",
		},
	},
	businessOperatingHours: {
		fontSize: "1.2rem",
		textTransform: "uppercase",
		color: greydark,
		fontWeight: 500,
		marginBottom: "0.3rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1rem",
		},
	},
	businessAddress: {
		width: "100%",
		fontSize: "1.2rem",
		lineHeight: "1.7rem",
		fontWeight: 300,
		color: "dimgrey",
	},

	businessPhone: {
		fontSize: "1.3rem",
		color: "dimgrey",
		textTransform: "uppercase",
		letterSpacing: "0.5px",
		fontWeight: 300,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},
	businessEmail: {
		fontSize: "1.3rem",
		color: "dimgrey",
		letterSpacing: "0.5px",
		fontStyle: "italic",
		fontWeight: 300,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},
	lDescText: {
		fontWeight: 300,
		fontSize: "1.2rem",
		letterSpacing: "0.3px",
	},
	titleDesc: {
		display: "flex",
		flexDirection: "column",
		marginBottom: "1rem",
	},
	titleDescTitle: {
		fontSize: "1rem",
		color: "grey",
	},
	titleDescDesc: {
		fontSize: "1.1rem",
		fontWeight: 500,
	},
	copyLinkBox: {
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		border: "1px solid black",
		borderRadius: "5px",
		padding: "0.1rem 0.8rem",
		marginTop: "0.5rem",
		userSelect: "none",
	},
	copyLinkIcon: {
		fontSize: "0.9rem",
		paddingRight: "0.3rem",
		cursor: "pointer",
	},
	copyLinkText: {
		fontSize: "1rem",
		fontWeight: 400,
		cursor: "pointer",
	},
	drawerListGroup: {
		alignSelf: "center",
		width: "95%",
		borderRadius: "1.2rem",
		padding: "0 1rem",
		boxSizing: "border-box",
		marginBottom: "1.5rem",
		backgroundColor: "#fff",
		[theme.breakpoints.down("xs")]: {
			marginBottom: "1.3rem",
		},
	},
});

class NavBar extends Component {
	state = {
		show: false,
		cartDrawer: false,
		bizDrawer: false,
		menu: false,
		langModal: null,
		isAddressOpened: false,
		cart: 0,
	};

	componentDidMount() {
		if (!isEmpty(this.props.cart.list)) {
			let cart = 0;
			this.props.cart.list.map((e, i) => {
				return (cart += e.quantity);
			});

			this.setState({
				cart: cart,
			});
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.notification.redirect !== this.props.notification.redirect) {
			if (this.props.notification.redirect === "AddCart" && this.props.isShowCart) {
				this.handleCartDrawer(true)();
			}
		}
		if (prevState.bizDrawer !== this.state.bizDrawer) {
			this.setState({
				isAddressOpened: false,
			});
		}
		if (prevState.cartDrawer !== this.state.cartDrawer) {
			this.props.onChangeCart(this.state.cartDrawer);
		}

		if (prevProps.cart.list !== this.props.cart.list) {
			let cart = 0;
			if (!isEmpty(this.props.cart.list)) {
				this.props.cart.list.map((e, i) => {
					return (cart += e.quantity);
				});
			}
			this.setState({
				cart: cart,
			});
		}
	}

	// handleMount = () => {
	// 	if (!isEmpty(this.props.cart.list)) {
	// 		let cart = 0;
	// 		this.props.cart.list.map((e, i) => {
	// 			return (cart += e.quantity);
	// 		});

	// 		this.setState({
	// 			cart: cart,
	// 		});
	// 	}
	// };

	handleNavBar = ({ previousPosition, currentPosition, event }) => {
		this.setState({
			show: currentPosition !== Waypoint.inside,
		});
	};

	handlePage = link => () => {
		if (link.includes("http")) {
			window.open(link);
		} else {
			if (this.props.router.asPath !== link) {
				this.props.setLoading(true);
			}

			this.props.router.push(link, undefined, { shallow: true });
		}
		this.handleBizDrawer(false)();
	};

	handleMenu = state => () => {
		this.setState({
			menu: state,
		});
	};
	handleCartDrawer = state => () => {
		this.setState({
			cartDrawer: state,
		});
	};

	handleBizDrawer = state => () => {
		this.setState({
			bizDrawer: state,
		});
	};
	handleLangModal = state => e => {
		this.setState({
			langModal: state ? e.currentTarget : null,
		});
	};

	handleCopyLink = () => {
		navigator.clipboard.writeText(window.location.href).then(
			() => {
				this.props.customNotification("Link copied", "success");
			},
			() => {
				this.props.customNotification("Error copying store link", "error");
			}
		);
	};

	render() {
		const { menu, show, cartDrawer, bizDrawer, isAddressOpened, cart } = this.state;
		const { classes, width, inverted, navs, noFixed, isShowCart, isShowMenu } = this.props;
		const { detail, isFetched } = this.props.business;
		const isActiveSubDrawer = isAddressOpened;

		const TitleDesc = props => (
			<div className={classes.titleDesc}>
				<Typography className={classes.titleDescTitle}>{props.title}</Typography>

				<InputBase className={classes.titleDescDesc} value={props.desc} multiline readOnly />
			</div>
		);

		const NavButton = props => {
			return this.props.router.asPath !== props.link ? (
				<Button
					disableRipple
					size="large"
					//	color="primary"
					color={props.last ? "secondary" : "primary"}
					variant={props.last ? "contained" : "text"}
					style={{
						margin: "0.5rem 0",
						marginLeft: props.last ? "0.5rem" : 0,
						height: "50px",
						padding: !props.last ? "0 1.5rem" : "0 2.5rem",
						fontFamily: "Inter, Arial",
						textTransform: "none",
						fontSize: "1.1rem",
						fontWeight: 500,
						letterSpacing: "0.5px",
						textTransform: "capitalize",
						borderRadius: "5px",
					}}
					onClick={props.action}
				>
					{props.icon ? props.icon : null}
					{props.name}
				</Button>
			) : null;
		};

		// const BizDrawer = (props) => {
		//   return (

		//   );
		// };

		const Content = props => (
			<div
				className={classes.content}
				//style={{ maxWidth: isShowMenu ? "500px" : "none" }}
			>
				<MenuRounded
					style={{ cursor: "pointer", fontSize: "2rem", color: secondary }}
					onClick={this.handleBizDrawer(true)}
				/>

				{/* {isFetched ? (
          <div
            className={classes.businessHeader}
            onClick={this.handlePage(`/`)}
          >
            <div className={classes.businessLogo}>
              <img
                style={{ maxHeight: "100%" }}
                src={detail && detail.logo ? detail.logo : "/images/noLogo.png"}
                alt={detail && detail.logo ? detail.logo : "noLogo"}
              />
            </div>

            <Typography className={classes.businessText}>
              {detail.name}
            </Typography>
          </div>
        ) : null} */}
				<div style={{ position: "relative", cursor: "pointer" }} onClick={this.handlePage("/bag")}>
					<LocalMallOutlined
						style={{
							fontSize: "1.8rem",
							color: secondary,
							padding: "10px 0",
							paddingLeft: "10px",
						}}
					/>
					{props.cart != 0 && (
						<div
							style={{
								width: "12px",
								height: "12px",
								borderRadius: "50%",
								display: "flex",
								justifyContent: "center",
								alignItems: "center",
								color: "white",
								backgroundColor: secondary,
								position: "absolute",
								right: -2,
								bottom: 11,
								fontSize: "8px",
							}}
						>
							{props.cart}
						</div>
					)}
				</div>
				{/* <ShareRounded
					style={{ cursor: "pointer", fontSize: "1.8rem", color: secondary }}
					onClick={this.handleCopyLink}
				/> */}
				{/* <div className={classes.copyLinkBox} onClick={this.handleCopyLink}>
          <FileCopyOutlined className={classes.copyLinkIcon} />
          <Typography className={classes.copyLinkText}>Copy link</Typography>
        </div> */}
			</div>
		);

		return (
			<div className={classes.root}>
				{noFixed ? (
					<Slide direction="down" in={show} mountOnEnter unmountOnExit>
						<Paper
							elevation={0}
							style={{ borderBottom: "1px solid gainsboro" }}
							square
							className={classes.navBar}
						>
							<Content />
						</Paper>
					</Slide>
				) : null}
				<Waypoint onPositionChange={this.handleNavBar}>
					<div className={classes.waypointTriggerBox} />
				</Waypoint>
				{!noFixed ? <div className={classes.clearContainer} /> : null}
				{noFixed ? (
					<Content />
				) : (
					<Paper
						square
						className={classes.navBar}
						elevation={0}
						style={{
							borderBottomWidth: "1px",
							borderBottomStyle: "solid",
							borderBottomColor: show ? "darkgrey" : "transparent",
						}}
					>
						<Content cart={cart} />
					</Paper>
				)}
				{/* {isShowMenu ? ( */}
				<Drawer
					anchor="left"
					open={bizDrawer}
					onClose={this.handleBizDrawer(false)}
					classes={{ paper: classes.bizDrawer }}
				>
					<div className={classes.businessDrawer}>
						<div className={classes.businessDrawerHeader}>
							<Typography onClick={this.handleBizDrawer(false)} className={classes.closeIcon}>
								Close
							</Typography>
							{/* <Zoom in={bizDrawer} timeout={{ enter: 500 }}>
                <CloseIcon
                  className={classes.closeIcon}
                  onClick={this.handleBizDrawer(false)}
                />
              </Zoom> */}
							<div className={classes.businessImgContainer} onClick={this.handlePage("/")}>
								<img
									className={classes.businessImg}
									src={!isEmpty(detail.logo) ? detail.logo : "/images/noLogo.png"}
								/>
							</div>
							<Typography className={classes.businessName}>{detail.name}</Typography>
						</div>
						<div className={classes.storeDetails}>
							<div className={classes.drawerListGroup}>
								<DrawerList text="Home" onClick={this.handlePage("/")} />
								<DrawerList last text="Contact us" onClick={this.handlePage("/contact-us")} />
							</div>
							<div className={classes.drawerListGroup}>
								{!isEmpty(detail.address) ? (
									<DrawerList
										last
										text="Address"
										onExpandChange={state => {
											this.setState({ isAddressOpened: state });
										}}
										collapsedChild={
											<InputBase
												className={classes.businessAddress}
												value={`${detail.address.address_1} ${detail.address.address_2 || ""} ${
													detail.address.address_3 || ""
												} ${detail.address.address_4 || ""}, ${detail.address.postcode} ${
													detail.address.city
												}, ${detail.address.state}, ${detail.address.country}`}
												multiline
												readOnly
											/>
										}
									/>
								) : null}
							</div>
						</div>
						<StoreUpTag />
					</div>
				</Drawer>
				{/* ) : null} */}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	business: state.business,
	item: state.item,
	notification: state.notification,
	cart: state.cart,
});

export default connect(mapStateToProps, { setLoading, customNotification })(
	withStyles(styles)(withWidth()(withRouter(NavBar)))
);
