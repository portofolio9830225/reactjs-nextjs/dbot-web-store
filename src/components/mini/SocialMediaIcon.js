import React, { Component } from "react";
import { Tooltip, withStyles, withWidth } from "@material-ui/core";
import { greydark, greylight } from "../../utils/ColorPicker";

const styles = theme => ({
	iconContainer: {
		paddingRight: "1.2rem",
		paddingLeft: 0,
		paddingTop: "5px",
	},
});

class SocialMediaIcon extends Component {
	state = {
		type: this.props.type,
	};

	iconName = () => {
		switch (this.state.type) {
			case "facebook":
				return "fa-facebook-f";
			case "twitter":
				return "fa-twitter";
			case "instagram":
				return "fa-instagram";
			case "youtube":
				return "fa-youtube";
			case "tiktok":
				return "fa-tiktok";
		}
	};

	handleSocMedPage = () => {
		let start = "";
		switch (this.state.type) {
			case "facebook":
				start = "https://facebook.com";
				break;
			case "twitter":
				start = "https://twitter.com";
				break;
			case "instagram":
				start = "https://www.instagram.com";
				break;
			case "youtube":
				start = "https://www.youtube.com/channel";
				break;
			case "tiktok":
				start = "https://www.tiktok.com";
				break;
		}
		// let link = `${start}/${this.props.link}`;
		let link = `${this.props.link}`;
		if (!link.startsWith("http")) {
			link = `https://${link}`;
		}
		window.open(link);
	};

	render() {
		const { width, classes } = this.props;
		return (
			// <Tooltip title={this.props.link}>
			<div className={classes.iconContainer} onClick={this.handleSocMedPage}>
				<i class={`fab ${this.iconName()}`} style={{ fontSize: width === "xs" ? "1.2rem" : "1.4rem" }} />
			</div>
			// </Tooltip>
		);
	}
}

export default withStyles(styles)(withWidth()(SocialMediaIcon));
