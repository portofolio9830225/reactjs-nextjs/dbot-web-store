import React, { Component } from "react";
import { Typography } from "@material-ui/core";
import { black, greyblack, greylight } from "../../utils/ColorPicker";
import isEmpty from "../../utils/isEmpty";

class PriceList extends Component {
	render() {
		let { end, title, price, emptyText } = this.props;
		return (
			<div
				style={{
					display: "flex",
					flexDirection: "row",
					justifyContent: "space-between",
					width: "100%",
					marginBottom: "0.7rem",
				}}
			>
				<Typography
					style={{
						fontSize: "1rem",
						color: end ? black : greyblack,
						fontWeight: end ? 700 : 400,
						textTransform: end ? "uppercase" : "none",
						boxSizing: "border-box",
						paddingRight: "1.5rem",
					}}
				>
					{title}
				</Typography>
				<div>
					{!isEmpty(price) ? (
						<div
							style={{
								display: "flex",
								flexDirection: "row",
								justifyContent: "space-between",
								width: end ? "auto" : "100px",
							}}
						>
							<Typography
								style={{
									color: end ? black : greyblack,
									fontWeight: end ? 700 : 300,
									fontSize: end ? "1.8rem" : "1.1rem",
								}}
							>
								RM
							</Typography>
							<Typography
								style={{
									color: end ? black : greyblack,
									fontWeight: end ? 700 : 300,
									fontSize: end ? "1.8rem" : "1.1rem",
								}}
							>
								{price.toFixed(2)}
							</Typography>
						</div>
					) : (
						<Typography
							style={{
								color: end ? black : greyblack,
								fontWeight: end ? 700 : 300,
								fontSize: end ? "1.8rem" : "1.1rem",
								color: greylight,
							}}
						>
							{emptyText}
						</Typography>
					)}
				</div>
			</div>
		);
	}
}

export default PriceList;
