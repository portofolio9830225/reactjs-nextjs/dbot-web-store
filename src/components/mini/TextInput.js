import React from "react";

import { TextField } from "@material-ui/core";
import { connect } from "react-redux";
import isEmpty from "../../utils/isEmpty";

class TextInput extends React.Component {
  state = {
    errorKey: this.props.errorkey || "",
    errorText: "",
  };

  componentDidMount() {
    if (!isEmpty(this.props.error.errInput)) {
      let err = this.props.error.errInput.find(
        (e) => e.param === this.state.errorKey
      );

      this.setState({
        errorText: !isEmpty(err) ? err.error : "",
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.error.errInput !== this.props.error.errInput &&
      !isEmpty(this.props.error.errInput)
    ) {
      let err = this.props.error.errInput.find(
        (e) => e.param === this.state.errorKey
      );

      this.setState({
        errorText: !isEmpty(err) ? err.error : "",
      });
    }
    if (prevProps.value !== this.props.value) {
      this.setState({
        errorText: "",
      });
    }
  }

  render() {
    const { children, error, ...rest } = this.props;
    const { errorText } = this.state;

    return (
      <TextField {...rest} error={!isEmpty(errorText)} helperText={errorText}>
        {children ? children : null}
      </TextField>
    );
  }
}

const mapStateToProps = (state) => ({
  error: state.error,
});

export default connect(mapStateToProps)(TextInput);
// export default TextInput;
