import React, { Component } from "react";
import { withRouter } from "next/router";

import { withStyles } from "@material-ui/core/styles";
import { Button, Typography } from "@material-ui/core";

import SectionColorBackground from "./SectionColorBackground";

const styles = (theme) => ({
  root: {
    position: "relative",
    width: "100%",
    padding: "4rem 1.5rem",
    display: "flex",
    boxSizing: "border-box",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down("xs")]: {
      padding: "2rem 1rem",
    },
  },
  content: {
    width: "100%",
    maxWidth: "1400px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  imgContainer: {
    //maxWidth: "150px",
    zIndex: 100,
    width: "40%",
    [theme.breakpoints.down("sm")]: {
      width: "70%",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "100%",
    },
  },
  image: {
    width: "100%",
  },
  text: {
    zIndex: 10,
    fontSize: "5rem",
    fontWeight: "bold",
    textAlign: "center",
    width: "80%",
    [theme.breakpoints.down("sm")]: {
      fontSize: "4.5rem",
      width: "100%",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "2.3rem",
    },
  },
  subtitle: {
    letterSpacing: "1.5px",
    zIndex: 10,
    fontSize: "1.3rem",
    textTransform: "uppercase",
    fontWeight: 300,
    marginBottom: "0.8rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1rem",
    },
  },
  desc: {
    width: "80%",
    zIndex: 10,
    fontSize: "1.5rem",
    marginTop: "1.4rem",
    textAlign: "center",
    fontWeight: 400,
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.7rem",
      marginTop: "2rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.2rem",
      width: "90%",
    },
  },
  button: {
    marginTop: "2rem",
    padding: "0 1.5rem",
    borderRadius: "5px",
    textTransform: "none",
    fontSize: "1.1rem",
    height: "50px",
    padding: "0 3rem",
    boxSizing: "border-box",
    fontWeight: 500,
    letterSpacing: "0.5px",
  },
});

class BigTitleDesc extends Component {
  handlePage = (link) => () => {
    if (link.includes("http")) {
      window.open(link);
    } else {
      this.props.router.push(link, undefined, { shallow: true });
    }
  };

  render() {
    const {
      classes,
      image,
      text,
      subtitle,
      desc,
      color,
      gradient,
      button,
      link,
      horizontal,
      vertical,
    } = this.props;

    return (
      <div className={classes.root}>
        <SectionColorBackground
          color={color}
          gradient={gradient}
          horizontal={horizontal}
          vertical={vertical}
        />
        <div className={classes.content}>
          {image && (
            <div className={classes.imgContainer}>
              <img src={image} alt={image} className={classes.image} />
            </div>
          )}
          {subtitle && (
            <Typography className={classes.subtitle}>{subtitle}</Typography>
          )}

          <Typography className={classes.text}>{text}</Typography>
          {desc && <Typography className={classes.desc}>{desc}</Typography>}
          {button && (
            <Button
              variant="contained"
              color="secondary"
              onClick={this.handlePage(button.link)}
              className={classes.button}
            >
              {button.text}
            </Button>
          )}
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(withRouter(BigTitleDesc));
