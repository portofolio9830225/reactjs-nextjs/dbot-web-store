import React, { Component } from "react";
import isEmpty from "../../utils/isEmpty";
import SwipeableViews from "react-swipeable-views";

import { withStyles } from "@material-ui/core/styles";
import { isMobile } from "react-device-detect";
import { secondary } from "../../utils/ColorPicker";

const styles = theme => ({
	root: {
		display: "flex",
		flexDirection: "column",
		width: "100%",
		boxSizing: "border-box",
		// padding: "0 2.5rem 1rem",
		padding: "0 0 1rem",
		maxWidth: "1300px",
		//justifyContent: "center",
		//marginBottom: "3rem",
		[theme.breakpoints.down("sm")]: {
			padding: "0 0 1rem",
		},
		position: "relative",
	},
	image: {
		width: "100%",
		height: "100%",
		objectFit: "contain",
	},
	mainImgContainer: {
		position: "relative",
		display: "flex",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		width: "100%",
		height: "100%",
		maxWidth: "600px",
		maxHeight: "600px",
	},

	imgListContainer: {
		margin: "1rem 0 0",
		display: "flex",
		flexDirection: "row",
		flexWrap: "wrap",
		width: "100%",
		justifyContent: "center",
	},
	altImgContainer: {
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		// transition: "all 0.3s",
		width: "50px",
		height: "50px",
		borderRadius: "25px",
		boxSizing: "border-box",
		overflow: "hidden",
		margin: "0.5rem",
		cursor: "pointer",
		borderStyle: "solid",
		borderWidth: "1px",
		"&:hover": {
			borderWidth: "2px",
		},
		[theme.breakpoints.down("sm")]: {
			width: "40px",
			height: "40px",
			borderRadius: "20px",
		},
	},
	cardDiscountedPrice: {
		padding: "0.5rem 2rem",
		background: "#FF9500",
		borderRadius: "5px",
		fontSize: "1.2rem",
		textAlign: "center",
		color: "white",
		position: "absolute",
		bottom: "78px",
		left: "50%",
		transform: "translateX(-50%)",
		[theme.breakpoints.down("sm")]: {
			bottom: "68px",
			fontSize: "1rem",
			padding: "0.5rem 1rem",
		},
	},
});

class ImageGrid extends Component {
	state = {
		mainImg: this.props.images[0].url_l,
		tempImg: "",
		selected: 1,
	};

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.images !== this.props.images) {
			this.setState({
				mainImg: this.props.images[0].url_l,
				selected: 1,
			});
		}
	}

	handleMainImg = (img, i) => () => {
		this.setState({
			mainImg: img,
			selected: i,
		});
	};

	handleImgHover =
		(data = "") =>
		() => {
			if (!isMobile) {
				this.setState({
					tempImg: data,
				});
			}
		};

	handleScrollView = index => {
		this.setState({
			selected: index + 1,
		});
	};

	render() {
		const { classes, images, handleClickImage, details } = this.props;
		const { mainImg, tempImg, selected } = this.state;

		const AltImage = props => (
			<div
				onClick={props.onClick}
				onMouseEnter={this.handleImgHover(props.index)}
				onMouseLeave={this.handleImgHover()}
				className={classes.altImgContainer}
				style={
					!props.active
						? {
								borderColor: "gainsboro",
						  }
						: { borderColor: secondary, borderWidth: "2px", padding: "0.1rem" }
				}
			>
				<img
					src={props.src}
					alt={props.alt}
					className={classes.image}
					style={{ width: "90%", height: "90%", borderRadius: "50%" }}
				/>
			</div>
		);

		if (!isEmpty(images)) {
			return (
				<div className={classes.root}>
					<SwipeableViews
						// className={classes.slideTrack}
						style={{ width: "100%" }}
						enableMouseEvents
						onChangeIndex={this.handleScrollView}
						index={!isEmpty(tempImg) ? tempImg - 1 : selected - 1}
					>
						{images.map((e, i) => {
							return (
								<div key={i} className={classes.productCardImgContainer}>
									<img
										src={e.url_l}
										style={{
											width: "100%",
											height: "100%",
											objectFit: "contain",
										}}
									/>
								</div>
							);
						})}
					</SwipeableViews>

					{!isEmpty(details.another_price) ? (
						<div style={images.length > 1 ? {} : { bottom: 0 }} className={classes.cardDiscountedPrice}>
							{parseInt(((details.another_price - details.price) / details.another_price) * 100)}% OFF
						</div>
					) : null}

					{images.length > 1 ? (
						<div className={classes.imgListContainer}>
							{images.map((e, i) => {
								let index = i + 1;
								return (
									<AltImage
										key={i}
										index={index}
										src={e.url_xs}
										alt={e.url_xs}
										active={selected === index}
										onClick={this.handleMainImg(e.url_l, index)}
									/>
								);
							})}
						</div>
					) : null}
				</div>
			);
		} else {
			return null;
		}
	}
}

export default withStyles(styles)(ImageGrid);
