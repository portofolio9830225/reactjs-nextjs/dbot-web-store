import { Typography } from "@material-ui/core";
import React, { Component } from "react";

class StoreUpTag extends Component {
  render() {
    return (
      <div
        style={{
          display: "flex",
          marginBottom: "0.5rem",
          position: "absolute",
          bottom: 0,
          cursor: "pointer",
          padding: "0.5rem 1rem",
        }}
        onClick={() => {
          window.open("https://storeup.io");
        }}
      >
        <Typography style={{ fontWeight: 300 }}>Powered by</Typography>
        <Typography style={{ fontWeight: 700, marginLeft: "0.3rem" }}>
          StoreUp
        </Typography>
      </div>
    );
  }
}

export default StoreUpTag;
