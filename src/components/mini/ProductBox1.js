import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "next/router";
import { connect } from "react-redux";
import { setCartItem } from "../../store/actions/cartAction";
import isEmpty from "../../utils/isEmpty";
import { withWidth, withMobileDialog, Typography, Grow, Zoom, Fade, Collapse } from "@material-ui/core";

import { AddRounded, BlockRounded, DeleteOutlineRounded, RemoveRounded } from "@material-ui/icons";
import { black, errorColor, greydark, secondary, secondaryLight } from "../../utils/ColorPicker";

const styles = theme => ({
	image: {
		width: "100%",
		height: "100%",
		objectFit: "cover",
	},
	root1: {
		display: "flex",
		flexDirection: "column",
		//marginBottom: "1rem",
		width: "100%",
	},
	root2: {
		display: "flex",
		flexDirection: "column",
		width: "48%",
	},
	borderBottomType1: {
		borderBottom: "1px solid gainsboro",
		margin: 0,
		alignSelf: "flex-end",
		width: "calc(100% - 85px)",
		[theme.breakpoints.down("xs")]: {
			width: "calc(100% - 75px)",
		},
	},
	productCard1: {
		padding: "0.8rem 0",
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		//borderRadius: "10px",
		//boxShadow: "3px 3px 15px #d6d6e3, -4px -4px 20px #ffffff",
		//borderBottom: "1px solid gb(238, 238, 238)",
		overflow: "hidden",
		// background: "linear-gradient(309.34deg, #F2F3F6 -13.68%, #E5E6EC 171.92%)",
		height: "80px",
		[theme.breakpoints.down("xs")]: {
			height: "70px",
		},
	},
	productCard2: {
		//marginBottom: "1.8rem",
		width: "100%",
		display: "flex",
		flexDirection: "column",
		//justifyContent: "space-between",
		alignItems: "center",
		borderRadius: "10px",
		//boxShadow: "3px 3px 15px #d6d6e3, -4px -4px 20px #ffffff",
		//overflow: "hidden",
		//background: "linear-gradient(309.34deg, #F2F3F6 -13.68%, #E5E6EC 171.92%)",
		// height: "100px",
		// [theme.breakpoints.down("xs")]: {
		//   height: "80px",
		// },
	},
	productCardImgBox1: {
		width: "80px",
		height: "80px",
		[theme.breakpoints.down("xs")]: {
			width: "70px",
			height: "70px",
		},
	},
	productCardImgBox2: {
		width: "100%",
		height: "100%",
	},
	productCardImgContainer1: {
		border: "1px solid gainsboro",
		position: "relative",
		borderRadius: "15px",
		overflow: "hidden",
		width: "100%",
		height: "100%",
	},
	productCardImgContainer2: {
		border: "1px solid gainsboro",
		position: "relative",
		borderRadius: "24px",
		overflow: "hidden",
		width: "100%",
		height: "100%",
	},
	productCardTextContainer: {
		boxSizing: "border-box",
		display: "flex",
		flexDirection: "column",
	},
	productCardActionContainer: {
		marginTop: "2.5rem",
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		[theme.breakpoints.down("xs")]: {
			marginTop: "1.5rem",
		},
	},
	title: {
		fontSize: "1.2rem",
		fontWeight: 500,
		letterSpacing: "0.8px",
		width: "100%",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},
	lDescText: {
		fontWeight: 300,
		fontSize: "1rem",
		letterSpacing: "0.3px",
		marginBottom: "0.1rem",
		color: greydark,
		[theme.breakpoints.down("xs")]: {
			fontSize: "0.9rem",
		},
	},
	price: {
		fontWeight: 400,
		color: greydark,
		fontSize: "1.1rem",
		marginRight: "8px",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1rem",
		},
	},
	another_price: {
		fontSize: "1.1rem",
		opacity: "0.5",
		color: "#777977",
		textDecoration: "line-through",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1rem",
		},
	},
	soldNumber: {
		fontWeight: 300,
		color: greydark,
		marginTop: "0.5rem",
		fontSize: "1rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "0.9rem",
		},
	},

	addIconContainer: {
		transition: "all 0.3s",
		cursor: "pointer",
		// boxShadow:
		//   "-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		marginRight: "10px",
		// background: "linear-gradient(145deg, #fcfdff, #dbdbe8)",
		// "&:active": {
		//   background: "linear-gradient(145deg, #dbdbe8, #fcfdff)",
		// },
		borderRadius: "50%",
		height: "50px",
		width: "50px",
		[theme.breakpoints.down("xs")]: {
			height: "35px",
			width: "35px",
		},
	},
	addIcon: {
		transition: "all 0.3s",
		color: greydark,
		fontWeight: "bold",
		fontSize: "1.8rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.3rem",
		},
	},

	counterClickerContainer: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		//alignItems: "center",
		alignItems: "flex-end",
		height: "100%",
	},

	counterText: {
		fontWeight: "bold",
		//textShadow: "1px 1px 2px #999, -1px -1px 2px #ffffff",
	},

	counterIconContainer: {
		transition: "all 0.3s",
		cursor: "pointer",
		// boxShadow:
		//   "-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		// background: "linear-gradient(145deg, #fcfdff, #dbdbe8)",
		// "&:active": {
		//   background: "linear-gradient(145deg, #dbdbe8, #fcfdff)",
		// },
		height: "40px",
		width: "40px",
		borderRadius: "20px",
		[theme.breakpoints.down("xs")]: {
			height: "36px",
			width: "36px",
			borderRadius: "16px",
		},
	},
	counterIcon: {
		color: secondary,
		fontWeight: "bold",
		fontSize: "1.7rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.5rem",
		},
	},
	maxLimitText: {
		fontWeight: 500,
		color: errorColor,
		textAlign: "center",
		letterSpacing: "0.1px",
		fontSize: "0.8rem",
		paddingTop: "0.2rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "0.65rem",
			paddingTop: "0.1rem",
		},
	},
	cardDiscountedPrice1: {
		background: "#FF9500",
		borderRadius: "5px",
		textAlign: "center",
		color: "white",
		position: "absolute",
		height: "12px",
		width: "50px",
		borderRadius: "6px",
		bottom: -8,
		zIndex: 10,
	},
	cardDiscountedPrice2: {
		background: "#FF9500",
		borderRadius: "12px",
		textAlign: "center",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		color: "white",
		position: "absolute",
		height: "24px",
		width: "66px",
		borderRadius: "12px",
		top: "8px",
		right: "8px",
		[theme.breakpoints.down("xs")]: {
			fontSize: "11px",
		},
	},
});

class ProductBox1 extends Component {
	state = {
		maxLimit: null,
		counter: this.props.counter,
		isCounterInactive: isEmpty(this.props.onClick),
		isCounterExpand: false,
		isCannotAdd: true,
	};
	counterExpandController = null;

	componentDidMount() {
		let itmNum = this.props.stock;
		if (!isEmpty(this.props.max) && this.props.stock > this.props.max) {
			itmNum = this.props.max;
		}
		this.setState({
			maxLimit: itmNum,
			isCannotAdd: !isEmpty(this.props.min) && this.props.min > itmNum,
		});
		if (!this.state.isCounterInactive) {
			this.updateCartCount();
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.cart.list !== this.props.cart.list) {
			this.updateCartCount();
		}
	}

	updateCartCount = () => {
		let foundInCart = this.props.cart.list.find(f => f.item_id === this.props.itmID);

		this.setState({
			counter: !isEmpty(foundInCart) ? foundInCart.quantity : 0,
		});
	};

	handleProductClick = () => {
		if (this.state.isCounterExpand) {
			this.setState(
				{
					isCounterExpand: false,
				},
				() => {
					clearTimeout(this.counterExpandController);
				}
			);
		} else {
			this.props.onClick();
		}
		// isCounterExpand
		// ? () => {

		//   }
		// : onClick
	};

	handleCounter = action => () => {
		clearTimeout(this.counterExpandController);
		let isClosed = false;
		let prevCounterAction = this.state.isCounterExpand;
		let prevCounter = this.state.counter;
		let numDecrease = this.state.counter - 1;
		let numIncrease = this.state.counter + 1;

		this.setState(
			{
				isCounterExpand: true,
			},
			() => {
				switch (action) {
					case "-":
						{
							this.setState(
								{
									counter: this.state.counter > 0 && numDecrease >= this.props.min ? numDecrease : 0,
								},
								() => {
									if (prevCounter !== this.state.counter) {
										this.props.setCartItem(
											this.props.cart.list,
											this.props.business.detail.business_id,
											{
												item_id: this.props.itmID,
												quantity: this.state.counter,
											}
										);
									}
									if (this.state.counter <= 0) {
										isClosed = true;
										this.setState({
											isCounterExpand: false,
										});
									}
								}
							);
						}
						break;
					case "+": {
						this.setState(
							{
								// counter:  (prevCounterAction || this.state.counter <= 0) &&
								//     numIncrease <= this.state.maxLimit
								//     ? numIncrease
								//     : this.state.counter

								counter: !this.state.isCannotAdd
									? this.state.counter <= 0 && this.props.min
										? this.props.min
										: (prevCounterAction || this.state.counter <= 0) &&
										  numIncrease <= this.state.maxLimit
										? numIncrease
										: this.state.counter
									: 0,
							},
							() => {
								if (prevCounter !== this.state.counter) {
									this.props.setCartItem(
										this.props.cart.list,
										this.props.business.detail.business_id,
										{
											item_id: this.props.itmID,
											quantity: this.state.counter,
										}
									);
								}
							}
						);
					}
				}
			}
		);

		if (!isClosed) {
			this.counterExpandController = setTimeout(() => {
				this.setState({ isCounterExpand: false });
			}, 2500);
		}
	};

	render() {
		const {
			width,
			classes,
			inactive,
			title,
			desc,
			img,
			price,
			onClick,
			stock,
			max,
			min,
			sold,
			type,
			isFirst,
			another_price,
		} = this.props;
		const { counter, isCounterInactive, isCounterExpand, maxLimit, isCannotAdd } = this.state;
		const isMobile = width === "xs";
		const isList = type === 1;
		const clickableWidth = isList ? (isMobile ? "calc(100% - 80px)" : "calc(100% - 100px)") : "100%";

		const SoldOutTag = () => {
			let small = isMobile || type == 1;

			return (
				<div
					style={{
						width: "100%",
						height: "100%",
						position: "absolute",
						top: 0,
						left: 0,
						display: "flex",
						alignItems: "center",
						justifyContent: "center",
					}}
				>
					<div
						style={{
							width: `100%`,
							height: `100%`,

							backgroundColor: "rgba(0,0,0,0.6)",
							display: "flex",
							alignItems: "center",
							justifyContent: "center",
						}}
					>
						<Typography
							style={{
								fontWeight: 500,
								color: "white",
								fontSize: small ? "0.7rem" : "1.5rem",
								letterSpacing: "0.5px",
							}}
						>
							Sold Out
						</Typography>
					</div>
				</div>
			);
		};

		const ProductTextInfo = ({ title, price, counter, sold, type, another_price }) => {
			return (
				<div
					//onClick={this.handleProductClick}
					className={classes.productCardTextContainer}
					style={{
						width: clickableWidth,
						padding: isList ? 0 : isMobile ? "0.5rem" : "1rem",
						paddingBottom: 0,
					}}
				>
					<Typography noWrap className={classes.title} style={{ color: stock <= 0 ? greydark : black }}>
						{title}
					</Typography>

					<div
						style={{
							width: "100%",
							display: "flex",
							flexDirection: "row",
						}}
					>
						{!isEmpty(price) ? (
							<Typography className={classes.price}>RM{price.toFixed(2)}</Typography>
						) : null}
						{!isEmpty(another_price) ? (
							<Typography className={classes.another_price}>RM{another_price.toFixed(2)}</Typography>
						) : null}

						{isCounterInactive && (
							<Typography className={classes.price} style={{ fontWeight: 700 }}>
								x {counter}
							</Typography>
						)}
					</div>
					{/* {!isCounterInactive && !isEmpty(sold) && (
            <Typography className={classes.soldNumber}>{sold} sold</Typography>
          )} */}
				</div>
			);
		};

		return (
			<div
				onClick={this.handleProductClick}
				className={isList ? classes.root1 : classes.root2}
				style={{ cursor: "pointer", transition: "all 0.25s", position: "relative" }}
			>
				{isList && !isFirst ? <div className={classes.borderBottomType1} /> : null}
				<div className={isList ? classes.productCard1 : classes.productCard2}>
					<div
						style={{
							display: "flex",
							flexDirection: "row",
							alignItems: "center",
							justifyContent: "space-between",
							transition: "all 0.3s",
							aspectRatio: 1 / 1,
							height: isList ? "100%" : "auto",
							width: isList
								? // isCounterHide
								  //   ? "95%"
								  //   :
								  `calc(100% - ${
										isCounterExpand ? (isMobile ? "105px" : "140px") : isMobile ? "60px" : "75px"
								  })`
								: "100%",
						}}
					>
						<div
							className={isList ? classes.productCardImgBox1 : classes.productCardImgBox2}
							style={{ display: "flex", justifyContent: "center", position: "relative" }}
						>
							<div
								//onClick={this.handleProductClick}
								className={isList ? classes.productCardImgContainer1 : classes.productCardImgContainer2}
							>
								{/* {(stock <= 0 || (!isEmpty(min) && min > stock)) && <SoldOutTag />} */}

								{inactive && <SoldOutTag />}

								<img src={isList ? img.url_s : img.url_m} className={classes.image} />
							</div>
							{!isEmpty(price) && !isEmpty(another_price) ? (
								<div className={isList ? classes.cardDiscountedPrice1 : classes.cardDiscountedPrice2}>
									<Typography style={{ fontSize: isList ? "9px" : "12px" }}>
										{parseInt(((another_price - price) / another_price) * 100)}% OFF
									</Typography>
								</div>
							) : null}
						</div>
						{isList ? (
							<Fade in={isList}>
								<ProductTextInfo
									title={title}
									price={price}
									counter={counter}
									sold={sold}
									type={type}
									another_price={another_price}
								/>
							</Fade>
						) : null}
					</div>
					{type !== 1 ? (
						<Fade in={type !== 1}>
							<ProductTextInfo
								title={title}
								price={price}
								another_price={another_price}
								counter={counter}
								sold={sold}
								type={type}
							/>
						</Fade>
					) : null}
					{isCounterInactive ? null : (
						<div
							className={classes.counterClickerContainer}
							style={{
								alignSelf: "flex-end",
								marginBottom: isList ? 0 : "0.5rem",
								width: isMobile ? "60px" : "75px",
							}}
						>
							<div className={classes.counterIconContainer}>
								<AddRounded
									className={classes.counterIcon}
									style={{ color: inactive ? "darkgrey" : secondary }}
								/>
							</div>
						</div>
					)}

					{/* {isCounterInactive ? null : inactive ? (
            <div
              className={classes.counterClickerContainer}
              style={{
                alignSelf: "flex-end",
                marginBottom: isList ? 0 : "0.5rem",
                width: isMobile ? "60px" : "75px",
              }}
            >
              <div className={classes.counterIconContainer}>
                <AddRounded
                  className={classes.counterIcon}
                  style={{ color: "darkgrey" }}
                />
              </div>
            </div>
          ) : (
            <div
              className={classes.counterClickerContainer}
              style={{
                alignSelf: "flex-end",
                marginBottom: isList ? 0 : "0.5rem",
                width: isCounterExpand
                  ? isMobile
                    ? "105px"
                    : "140px"
                  : isMobile
                  ? "60px"
                  : "75px",
              }}
            >
              <div
                style={{
                  transition: "all 0.25s",
                  display: "flex",
                  flexDirection: "row",
                  boxSizing: "border-box",
                  alignItems: "center",
                  padding: isCounterExpand ? "0.3rem" : 0,
                  justifyContent: isCounterExpand ? "space-between" : "center",
                  boxShadow: "none",
                  width: isCounterExpand
                    ? isMobile
                      ? "90px"
                      : "130px"
                    : isMobile
                    ? "36px"
                    : "40px",
                  height: isCounterExpand
                    ? isMobile
                      ? "calc(30px + 0rem)"
                      : "calc(40px + 0rem)"
                    : isMobile
                    ? "36px"
                    : "40px",
                  borderRadius: isCounterExpand
                    ? isMobile
                      ? "calc(15px + 0rem)"
                      : "calc(20px + 0rem)"
                    : isMobile
                    ? "18px"
                    : "20px",
                  backgroundColor: "rgba(229,229,234,1)",
                  // backgroundColor: isCounterExpand
                  //   ? "#fff"
                  //   : counter <= 0
                  //   ? "#fff"
                  //   : "#eeeef0",
                  border: isCounterExpand
                    ? "none"
                    : counter <= 0
                    ? "none"
                    : `1px solid ${secondary}`,
                }}
              >
                <Zoom in={isCounterExpand} mountOnEnter unmountOnExit>
                  <div
                    className={classes.counterIconContainer}
                    onClick={this.handleCounter("-")}
                  >
                    {(counter <= 1 || counter <= min) && (
                      <Zoom in={counter <= 1 || counter <= min}>
                        <DeleteOutlineRounded
                          className={classes.counterIcon}
                          style={{ color: errorColor }}
                        />
                      </Zoom>
                    )}
                    {!(counter <= 1 || counter <= min) && (
                      <Zoom in={!(counter <= 1 || counter <= min)}>
                        <RemoveRounded className={classes.counterIcon} />
                      </Zoom>
                    )}
                  </div>
                </Zoom>
                <Grow in={isCounterExpand} mountOnEnter unmountOnExit>
                  <Typography
                    className={classes.counterText}
                    style={{
                      fontSize: isMobile ? "0.9rem" : "1.2rem",
                      color: greydark,
                    }}
                  >
                    {counter}
                  </Typography>
                </Grow>
                <div
                  className={classes.counterIconContainer}
                  style={{
                    boxShadow: counter > 0 && !isCounterExpand && "none",
                  }}
                  onClick={this.handleCounter("+")}
                >
                  {counter <= 0 || isCounterExpand ? (
                    <Fade
                      in={counter <= 0 || isCounterExpand}
                      mountOnEnter
                      unmountOnExit
                    >
                      <div
                        style={{
                          width: "100%",
                          height: "100%",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                          alignSelf: "center",
                        }}
                        // onClick={this.handleCounter("+")}
                      >
                        <AddRounded className={classes.counterIcon} />
                      </div>
                    </Fade>
                  ) : (
                    <Fade
                      in={!(counter <= 0 || isCounterExpand)}
                      mountOnEnter
                      unmountOnExit
                    >
                      <Typography
                        style={{
                          fontSize: isMobile ? "0.9rem" : "1.2rem",
                          fontWeight: "bold",
                          //textShadow: "1px 1px 2px #999, -1px -1px 2px #ffffff",
                          color: secondary,
                        }}
                      >
                        {counter}
                      </Typography>
                    </Fade>
                  )}
                </div>
              </div>
              <Collapse in={counter >= maxLimit && isCounterExpand}>
                <Typography className={classes.maxLimitText}>
                  Max. quantity reached
                </Typography>
              </Collapse>
            </div>
          )} */}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	business: state.business,
	cart: state.cart,
	loading: state.loading,
	notification: state.notification,
	error: state.error,
});

export default connect(mapStateToProps, { setCartItem })(
	withStyles(styles)(withMobileDialog({ breakpoint: "xs" })(withRouter(withWidth()(ProductBox1))))
);
