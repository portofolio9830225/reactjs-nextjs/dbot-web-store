import React, { Component } from "react";
import ResizeObserver from "resize-observer-polyfill";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import isEmpty from "../utils/isEmpty";
import { withStyles } from "@material-ui/core/styles";

import { clearItem, getItemDetails } from "../store/actions/itemAction";
import { setProductBoxType } from "../store/actions/storeviewAction";
import { customNotification } from "../store/actions/notificationAction";
import { setLoading } from "../store/actions/loadingAction";

import {
	ChevronLeftRounded,
	ExpandLessRounded,
	FileCopyOutlined,
	FormatListBulletedRounded,
	GridOnRounded,
	SearchOutlined,
} from "@material-ui/icons";

import {
	black,
	grey,
	greydark,
	greylight,
	greywhite,
	mainBgColor,
	secondary,
	secondaryDark,
} from "../utils/ColorPicker";
import {
	withMobileDialog,
	Dialog,
	withWidth,
	Typography,
	InputAdornment,
	Fade,
	Slide,
	Collapse,
	Button,
	InputBase,
	IconButton,
	Tabs,
	Tab,
} from "@material-ui/core";

import PriceList from "../components/mini/PriceList";
import TextInput from "../components/mini/TextInput";
import ProductBox from "../components/mini/ProductBox1";
import { isSafari } from "react-device-detect";
import StoreUpTag from "../components/mini/StoreUpTag";
import SocialMediaIcon from "../components/mini/SocialMediaIcon";
import PageHelmet from "../components/mini/PageHelmet";
import EmptyList from "../components/mini/EmptyList";
import ListingsInCategory from "../components/mini/ListingsInCategory";
import { createOrder, getOrderBySessionID, setItemOfOrder } from "../store/actions/bookingAction";

const rootPaddingVertical = "12px";

const styles = theme => ({
	root: {
		flex: 1,
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		width: "100%",
		position: "relative",
	},
	main: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		width: "100%",
		height: "100%",
		// [theme.breakpoints.down("xs")]: {
		// 	width: "95%",
		// },
	},
	businessCard: {
		display: "flex",
		flexDirection: "column",
		margin: "1.5rem 0",
		width: "100%",
		[theme.breakpoints.down("xs")]: {
			padding: `0 ${rootPaddingVertical}`,
			boxSizing: "border-box",
		},
	},
	businessHeader: {
		display: "flex",
		flexDirection: "row",
		marginBottom: "1.5rem",
		//padding: "0 1.5rem",
		//alignItems: "center",
	},
	logoContainer: {
		border: "1px solid gainsboro",
		width: "30%",
		aspectRatio: 1,
		//height: "100px",
		borderRadius: "30px",
		overflow: "hidden",
		marginRight: "2rem",
		[theme.breakpoints.down("xs")]: {
			marginRight: "1rem",
			// width: "80px",
			// height: "80px",
		},
	},
	logo: {
		width: "100%",
		height: "100%",
	},
	businessName: {
		fontWeight: 500,
		fontSize: "2.2rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.5rem",
			//textAlign: "center",
		},
	},
	businessDesc: {
		color: greydark,
		fontSize: "1.2rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},
	businessSocialMedia: {
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		flexWrap: "wrap",
	},
	textField: {
		width: "100%",
		marginTop: "1.5rem",
	},
	sectionTitle: {
		color: black,
		fontSize: "1.5rem",
		fontWeight: 500,
		width: "100%",
		// marginTop: "3rem",
		marginBottom: "0.5rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.3rem",
			marginBottom: 0,
		},
	},
	listingSection: {
		display: "flex",
		flexDirection: "column",
		width: "100%",
		marginBottom: "5rem",
	},
	listingHeader: {
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		marginBottom: "1.5rem",
		[theme.breakpoints.down("xs")]: {
			padding: `0 ${rootPaddingVertical}`,
			boxSizing: "border-box",
		},
	},
	listingContent: {
		display: "flex",
		flexWrap: "wrap",
		justifyContent: "space-between",
		position: "relative",
		[theme.breakpoints.down("xs")]: {
			padding: `0 ${rootPaddingVertical}`,
			boxSizing: "border-box",
		},
	},
	categoryContent: {
		display: "flex",
		flexWrap: "wrap",
		justifyContent: "space-between",
		position: "relative",
	},
	emptyList: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		margin: "4rem 0",
		width: "100%",
	},
	emptyListTitle: {
		fontWeight: 500,
		width: "80%",
		maxWidth: "600px",
		fontSize: "2rem",
		textAlign: "center",
		color: greydark,
	},
	emptyListText: {
		width: "80%",
		maxWidth: "600px",
		marginBottom: "2rem",
		fontSize: "1.3rem",
		textAlign: "center",
		color: greydark,
		[theme.breakpoints.down("sm")]: {
			marginBottom: "1rem",
		},
	},

	backIconContainer: {
		transition: "all 0.3s",
		cursor: "pointer",
		boxShadow:
			"-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
		// boxShadow: "2px 2px 4px #e3eeff, -2px -2px 4px #ffffff",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		height: "40px",
		minWidth: "40px",
		borderRadius: "50%",
		background: "linear-gradient(145deg, #fcfdff, #dbdbe8)",
		"&:active": {
			background: "linear-gradient(145deg, #dbdbe8, #fcfdff)",
		},
	},
	backIcon: {
		color: greydark,
		fontSize: "2rem",
	},
	closeIcon: {
		alignSelf: "flex-end",
		padding: "1rem",
		paddingRight: "0.3rem",
		fontSize: "2.5rem",
		marginBottom: "1rem",
		cursor: "pointer",
	},
	businessDrawer: {
		width: "100%",
		height: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "space-between",
		marginBottom: 20,
		padding: "0 3rem",
		boxSizing: "border-box",
		[theme.breakpoints.down("xs")]: {
			height: isSafari ? "90%" : "100%",
			padding: "0 1rem",
		},
	},
	businessImgContainer: {
		boxShadow:
			"-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
		width: "125px",
		height: "125px",
		borderRadius: "50%",
		margin: "1rem",
		[theme.breakpoints.down("xs")]: {
			alignSelf: "center",
			width: "75px",
			height: "75px",
		},
	},
	businessImg: {
		maxWidth: "100%",
		maxHeight: "100%",
		borderRadius: "50%",
	},
	businessTitle: {
		fontSize: "2rem",
		fontWeight: 600,
		textAlign: "center",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.4rem",
		},
	},
	contactButtonHeader: {
		height: "auto",
		[theme.breakpoints.down("xs")]: {
			fontSize: "0.8rem",
		},
	},
	businessDrawerHeader: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	storeDetails: {
		margin: "1rem 0",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		cursor: "pointer",
		width: "100%",
	},
	copyLinkBox: {
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		border: "1px solid black",
		borderRadius: "5px",
		padding: "0.1rem 0.8rem",
		marginTop: "0.5rem",
		userSelect: "none",
	},
	copyLinkIcon: {
		fontSize: "0.9rem",
		paddingRight: "0.3rem",
		cursor: "pointer",
	},
	copyLinkText: {
		fontSize: "1rem",
		fontWeight: 400,
		cursor: "pointer",
	},
	productBoxSelectorIcon: {
		fontSize: "1.8rem",
		transition: "all 0.3s",
	},
	categoryTabFirst: {
		minWidth: 0,
		[theme.breakpoints.up("sm")]: {
			paddingLeft: 0,
		},
	},
	categoryTab: {
		minWidth: 0,
	},
	categoryTabLast: {
		minWidth: 0,
		[theme.breakpoints.up("sm")]: {
			paddingRight: 0,
		},
	},
	selectedTab: {
		fontWeight: 600,
	},

	DialogRoot: {
		backgroundColor: "rgba(0,0,0,0)",
		boxShadow: "none",
		display: "flex",
		borderRadius: 0,
	},
	DialogRootItem: {
		backgroundColor: mainBgColor,
		boxShadow: "none",
		display: "flex",
		height: "100%",
		// height: "100vh",
		borderRadius: 0,
		position: "relative",
	},
	DialogPaperScrollPaper: {
		maxHeight: "100%",
		// maxHeight: "100vh",
		display: "flex",
		alignItems: "center",
	},
	BankMenuItem: {
		paddingTop: "1.5rem",
		paddingBottom: "1.5rem",
	},
});

const SEARCH_MIN_LIMIT = 2;
class Business extends Component {
	state = {
		name: "",
		phone: "",
		step: 1,
		cartHeight: 0,
		cartExpanded: false,
		subtotal: 0,
		selectedCategory: 0,
		searchText: "",
		isFirstView: true,
		listings: [],
		searchListings: [],
	};

	calcSubtotal = () => {
		let subtotal = 0;
		this.props.cart.list.map((e, i) => {
			let price = e.details.price * e.quantity;
			subtotal += price;
		});
		this.setState({
			subtotal,
		});
	};

	updateListings = arr => {
		this.setState({
			listings: arr,
		});
	};

	componentDidMount() {
		this.props.setLoading(false);
		this.setResizer();

		let savedProductBoxType = localStorage.getItem("@storeup:productBoxType");
		this.handleProductBox(!isEmpty(savedProductBoxType) ? parseInt(savedProductBoxType) : 1)();

		this.updateListings(this.props.business.listings);
		this.calcSubtotal();
		this.setState({
			isFirstView: !isEmpty(this.props.business.categories),
		});
		// console.log(this.props.SEO);
	}

	componentDidUpdate(prevProps, prevState) {
		if (!isEmpty(prevProps.item.itemPage.details) && isEmpty(this.props.item.itemPage.details)) {
			this.setResizer();
		}
		if (isEmpty(prevProps.item.itemPage.details) && !isEmpty(this.props.item.itemPage.details)) {
			this.setState({
				cartExpanded: false,
			});
		}
		if (isEmpty(prevProps.cart.list) && !isEmpty(this.props.cart.list)) {
			this.calcSubtotal();
		}
		if (this.state.selectedCategory !== prevState.selectedCategory) {
			let num = this.state.selectedCategory;
			if (num === 0) {
				this.updateListings(this.props.business.listings);
			} else {
				this.updateListings(this.props.business.categories[num - 1].listings);
			}
		}
		if (prevState.searchText !== this.state.searchText) {
			let { searchText } = this.state;
			let arr = [];
			let num = this.state.selectedCategory;
			if (num === 0) {
				arr = [...this.props.business.listings];
			} else {
				arr = [...this.props.business.categories[num - 1].listings];
			}

			this.setState({
				searchListings:
					searchText.length >= SEARCH_MIN_LIMIT
						? arr.filter(f => {
								let lcSearchText = searchText.toLowerCase();
								let name = f.name.toLowerCase();

								return name.includes(lcSearchText);
						  })
						: [],
			});
		}
		if (isEmpty(prevProps.notification.redirect) && !isEmpty(this.props.notification.redirect)) {
			setTimeout(() => {
				let { redirect } = this.props.notification;
				let urlParams = new URLSearchParams(window.location.search);
				let oID = urlParams.get("order_ref");

				if (redirect === "setItemOfOrder" || redirect === "createOrder") {
					this.props.setLoading(false);

					window.location.href = `${
						process.env.NODE_ENV === "production" ? "https://checkout.storeup.io" : "http://localhost:3002"
					}/${oID}?r=1&wa=1`;
				}
			}, 300);
		}
	}

	setResizer = () => {
		let cartEl = document.getElementById("cartCard");
		this.resizeObserver.observe(cartEl);
	};

	resizeObserver = new ResizeObserver(entries =>
		this.setState({
			cartHeight: entries[0].target.clientHeight,
		})
	);

	handleBack = () => {
		if (this.state.shipType === 1) {
			this.setState({
				step: 1,
			});
		} else {
			this.setState({
				step: this.state.step - 1,
			});
		}
	};

	handlePage = link => () => {
		if (link.includes("http")) {
			window.open(link);
		} else {
			if (this.props.router.asPath !== link) {
				this.props.setLoading(true);
			}

			this.props.router.push(link, undefined, { shallow: true });
		}
	};

	handleSelect = name => event => {
		this.setState({ [name]: event.target.value });
	};

	handleText =
		(name, limit = 999) =>
		event => {
			let val = event.target.value;

			if (val.length <= limit) {
				this.setState({ [name]: val });
			}
		};

	handleItem =
		(id = null) =>
		() => {
			if (!isEmpty(id)) {
				let link = `/p/${id}`;

				//this.props.getItemDetails(id, this.props.business.detail.business_id);
				this.handlePage(link)();
			} else {
				this.props.clearItem();
			}
		};

	handleProductBox = type => () => {
		this.props.setProductBoxType(type);
	};

	handleTabChange = (ev, val) => {
		this.handleClickCategoryTab(val)();
	};

	handleClickCategoryTab = val => () => {
		this.setState({
			selectedCategory: val,
			isFirstView: false,
		});
	};

	handleSubmitWhatsapp = async () => {
		this.props.setLoading(true);
		let products = [];

		this.props.cart.list.map(c => {
			products.push({
				product_ref: c.item_id,
				variant_id: c.variant_number,
				quantity: c.quantity,
			});
		});
		let data = {
			products,
			business_id: this.props.business.detail.id,
		};

		let oID = await this.props.getOrderBySessionID(sessionStorage.getItem("storeup:sessionID"));

		if (!isEmpty(oID)) {
			this.props.setItemOfOrder(data, oID);
		} else {
			this.props.createOrder(data, this.props.customer.sessionID);
		}
	};

	render() {
		const { width, classes, SEO } = this.props;
		const { cartExpanded, step, selectedCategory, listings, searchText, searchListings, isFirstView } = this.state;
		const { productBoxType } = this.props.storeview;
		const { detail, categories } = this.props.business;
		const { details } = this.props.item.itemPage;
		const { list } = this.props.cart;
		const isShowCart = isEmpty(details) && !isEmpty(list);
		const isProductList = productBoxType === 1;

		const EmptyProduct = () => {
			return <EmptyList title="Coming Soon" desc="Exciting products will be here soon. Stay tuned!" />;
		};

		return (
			<div className={classes.root} style={{ marginBottom: isShowCart ? this.state.cartHeight : 0 }}>
				<PageHelmet
					metadata={{
						title: `${SEO.name}`,
						description: `${SEO.description} [Powered by StoreUp]`,
					}}
				/>
				{step === 1 && (
					<Fade in={step === 1}>
						<div className={classes.root}>
							<div className={classes.main}>
								<div className={classes.businessCard}>
									<div className={classes.businessHeader}>
										<div className={classes.logoContainer}>
											<img
												className={classes.logo}
												src={!isEmpty(detail.logo) ? detail.logo : "/images/noLogo.svg"}
											/>
										</div>
										<div
											style={{
												display: "flex",
												flexDirection: "column",
												alignItems: "flex-start",
												justifyContent: "space-between",
											}}
										>
											<div
												style={{
													display: "flex",
													flexDirection: "column",
													alignItems: "flex-start",
												}}
											>
												<Typography className={classes.businessName}>{detail.name}</Typography>
												<div className={classes.businessSocialMedia}>
													{!isEmpty(detail.social_media)
														? detail.social_media.map((e, i) => {
																return (
																	<SocialMediaIcon
																		key={i}
																		link={e.link}
																		type={e.type}
																	/>
																);
														  })
														: null}
												</div>
											</div>
											<Button
												color="secondary"
												className={classes.contactButtonHeader}
												onClick={this.handlePage("/contact-us")}
											>
												Contact us
											</Button>
										</div>
									</div>
									<InputBase
										className={classes.businessDesc}
										value={detail.description}
										multiline
										readOnly
									/>
								</div>

								<div className={classes.listingSection}>
									<div className={classes.listingHeader}>
										<div
											style={{
												flex: 1,
												height: "42px",
												borderRadius: "21px",
												marginRight: "16px",
												border: `1px solid ${greywhite}`,
												display: "flex",
												flexDirectiion: "row",
												alignItems: "center",
												justifyContent: "space-between",
												boxSizing: "border-box",
												padding: "0 10px",
											}}
										>
											<SearchOutlined style={{ color: grey, fontSize: "22px" }} />
											<InputBase
												value={searchText}
												onChange={this.handleText("searchText")}
												placeholder="Search"
												style={{
													flex: 1,
													boxSizing: "border-box",
													paddingLeft: "10px",
													fontSize: "17px",
												}}
											/>
										</div>

										<div
											style={{
												display: "flex",
												flexDirection: "row",
												alignItems: "center",
											}}
										>
											<IconButton size="small" onClick={this.handleProductBox(1)}>
												<FormatListBulletedRounded
													className={classes.productBoxSelectorIcon}
													style={{
														color: isProductList ? secondary : greylight,
													}}
												/>
											</IconButton>
											<IconButton
												size="small"
												onClick={this.handleProductBox(2)}
												style={{ marginLeft: "0.7rem" }}
											>
												<GridOnRounded
													className={classes.productBoxSelectorIcon}
													style={{
														color: isProductList ? greylight : secondary,
													}}
												/>
											</IconButton>
										</div>
									</div>
									<Tabs
										value={selectedCategory}
										onChange={this.handleTabChange}
										indicatorColor="transparent"
										textColor="secondary"
										variant="scrollable"
										scrollButtons="auto"
									>
										<Tab
											label="All products"
											disableRipple
											classes={{
												root: classes.categoryTabFirst,
												selected: classes.selectedTab,
											}}
										/>
										{categories.map((c, i) => {
											let index = i + 1;
											return (
												<Tab
													key={index}
													label={c.name}
													disableRipple
													classes={{
														root:
															index === categories.length
																? classes.categoryTabLast
																: classes.categoryTab,
														selected: classes.selectedTab,
													}}
												/>
											);
										})}
									</Tabs>
									{!isEmpty(listings) ? (
										<div
											className={classes.listingContent}
											style={{
												flexDirection: isProductList ? "column" : "row",
											}}
										>
											{!isEmpty(searchText) && searchText.length >= SEARCH_MIN_LIMIT ? (
												!isEmpty(searchListings) ? (
													searchListings.map((e, i) => {
														return (
															<ProductBox
																key={i}
																inactive={!e.stockAvailable}
																isFirst={i == 0}
																title={e.name}
																desc={e.description}
																img={!isEmpty(e.images) ? e.images[0] : null}
																price={e.price}
																another_price={e.another_price}
																stock={e.total_stock}
																max={e.max_limit}
																min={e.min_limit}
																sold={e.number_sold}
																itmID={e.ref}
																onClick={this.handleItem(e.ref, e.name)}
																type={productBoxType}
															/>
														);
													})
												) : (
													<EmptyProduct />
												)
											) : isFirstView ? (
												<>
													<ListingsInCategory
														name="All products"
														onPress={this.handleClickCategoryTab(0)}
														listings={listings}
														productBoxType={productBoxType}
														className={classes.categoryContent}
													/>
													{categories.map((c, i) => {
														return (
															<ListingsInCategory
																key={i}
																name={c.name}
																onPress={this.handleClickCategoryTab(i + 1)}
																listings={c.listings}
																productBoxType={productBoxType}
																className={classes.categoryContent}
															/>
														);
													})}
												</>
											) : (
												listings.map((e, i) => {
													return (
														<ProductBox
															key={i}
															inactive={!e.stockAvailable}
															isFirst={i == 0}
															title={e.name}
															desc={e.description}
															img={!isEmpty(e.images) ? e.images[0] : null}
															price={e.price}
															another_price={e.another_price}
															stock={e.total_stock}
															max={e.max_limit}
															min={e.min_limit}
															sold={e.number_sold}
															itmID={e.ref}
															onClick={this.handleItem(e.ref, e.name)}
															type={productBoxType}
														/>
													);
												})
											)}
										</div>
									) : (
										<EmptyProduct />
									)}
								</div>

								<Fade in={!isShowCart}>
									<StoreUpTag />
								</Fade>
							</div>
						</div>
					</Fade>
				)}

				<Slide direction="up" in={isShowCart}>
					<div
						id="cartCard"
						style={{
							position: "fixed",
							bottom: 0,
							zIndex: 150,
							backgroundColor: "rgba(255,255,255,0.7)",
							//opacity: 0.5,
							width: "100%",
							maxWidth: "610px",
							display: "flex",
							flexDirection: "column",
							alignItems: "center",
							boxShadow: "0px -2px 2px rgba(0, 0, 0, 0.1)",
							// borderRadius: "1rem 1rem 0px 0px",
							borderTop: "1px solid gainsboro",
							boxSizing: "border-box",
							padding: "0 1.5rem 1rem",
							//padding: "0 1.5rem 3.5rem",
						}}
					>
						<div
							style={{
								display: "flex",
								width: "100%",
								flexDirection: "column",
								alignItems: "center",
								//padding: "1rem 0 0",
								//marginBottom: "0.5rem",
								cursor: "pointer",
							}}
							onClick={() =>
								this.setState({
									cartExpanded: !this.state.cartExpanded,
								})
							}
						>
							<ExpandLessRounded
								style={{
									fontSize: "2.5rem",
									transition: "all 0.3s",
									color: greydark,
									transform: `rotate(${this.state.cartExpanded ? "-180deg" : "0deg"})`,
								}}
							/>
							{/* <div
             
              /> */}
							<Collapse in={cartExpanded}>
								<div style={{ width: "100%" }}>
									{list.map((e, i) => {
										let price = e.details.price * e.quantity;
										return (
											<PriceList
												key={i}
												title={`${e.quantity}x ${e.details.name} ${
													!isEmpty(e.variant_name) ? `(${e.variant_name})` : ""
												}`}
												price={price}
											/>
										);
									})}
								</div>
							</Collapse>
						</div>

						<PriceList end title="Subtotal" price={this.state.subtotal} />

						<div
							style={{
								display: "flex",
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center",
								//paddingTop: "1rem",
								width: "100%",
							}}
						>
							<Button
								onClick={this.handleSubmitWhatsapp}
								variant="outlined"
								color="primary"
								style={{
									display: "flex",
									alignItems: "center",
									justifyContent: "center",
									flex: 3,
									marginRight: "1rem",
									backgroundColor: "white",
								}}
							>
								<i class="fab fa-whatsapp" style={{ fontSize: "2.3rem" }} />
							</Button>
							<Button
								onClick={this.handlePage("/bag")}
								variant="contained"
								color="secondary"
								style={{ flex: 9 }}
							>
								Review Bag
							</Button>
						</div>
					</div>
				</Slide>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	item: state.item,
	business: state.business,
	customer: state.customer,
	booking: state.booking,
	cart: state.cart,
	storeview: state.storeview,
	notification: state.notification,
	error: state.error,
});

export default connect(mapStateToProps, {
	getItemDetails,
	clearItem,
	getOrderBySessionID,
	setItemOfOrder,
	createOrder,
	setProductBoxType,
	customNotification,
	setLoading,
})(withStyles(styles)(withMobileDialog({ breakpoint: "xs" })(withWidth()(withRouter(Business)))));
