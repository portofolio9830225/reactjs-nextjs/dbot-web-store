import React, { Component } from "react";
import { connect } from "react-redux";
import { setLoading } from "../store/actions/loadingAction";
import { customNotification } from "../store/actions/notificationAction";
import { Typography, Fade, withStyles } from "@material-ui/core";
import {
	black,
	grey,
	greydark,
	greylight,
	greywhite,
	mainBgColor,
	secondary,
	primary,
	errorColor,
	greyblack,
} from "../utils/ColorPicker";
import isEmpty from "../utils/isEmpty";
import { TextField, Slide, Backdrop } from "@material-ui/core";
import { RemoveRounded, AddRounded, CloseRounded, Close } from "@material-ui/icons";
import { getBusinessPromo } from "../store/actions/businessAction";
import { setCartItem } from "../store/actions/cartAction";
import { Button } from "@material-ui/core";
import { withRouter } from "next/router";
import productLimiter from "../utils/productLimiter";
import {
	createOrder,
	editOrderNotes,
	editOrderPromo,
	getBookDetails,
	getOrderBySessionID,
	setItemOfOrder,
} from "../store/actions/bookingAction";
// import StoreUpLogo from "../../public/images/storeuplogo.svg";

export class Bag extends Component {
	state = {
		isVocer: false,
		isPromo: false,
		promo: "",
		notes: "",
		subtotal: 0,

		productCardInfo: null,
		clickedVariant: null,
		selectedVariant: null,
		counter: 1,
		maxLimit: 999,
		minLimit: 0,
	};

	async componentDidMount() {
		this.props.setLoading(false);

		let orderRef = await this.props.getOrderBySessionID(sessionStorage.getItem("storeup:sessionID"));
		if (!isEmpty(orderRef)) {
			let urlParams = new URLSearchParams(window.location.search);
			urlParams.set("order_ref", orderRef);
			const urlQueryString = urlParams.toString();
			window.history.replaceState(
				null,
				null,
				`${window.location.pathname}${!isEmpty(urlQueryString) ? `?${urlQueryString}` : ""}`
			);
		}

		// this.props.setLoading(true);
		// const urlParams = new URLSearchParams(window.location.search);
		// const oID = urlParams.get("order_ref");

		// let arr = [];

		// if (!isEmpty(oID)) {
		// 	const order = await this.props.getBookDetails(oID);

		// 	console.log(order)
		// } else {
		// 	arr = [...this.props.cart.list];
		// }
		let arr = [...this.props.cart.list];

		arr.map((e, i) => {
			let itemIndex = this.props.business.listings.findIndex(f => {
				let varIndex = f.variants.findIndex(v => v.id == e.variant_number);
				const varFound = varIndex !== -1;

				return f.ref === e.item_id && varFound;
			});

			const itemAvailable = itemIndex !== -1;
			if (!itemAvailable) {
				e.available = false;
			}
		});

		this.props.setCartItem(arr, this.props.business.detail.business_id);
	}

	componentDidUpdate(prevProps, prevState) {
		if (!isEmpty(prevState.productCardInfo) && isEmpty(this.state.productCardInfo)) {
			this.setState({
				selectedVariant: null,
				clickedVariant: null,
				counter: 1,
				maxLimit: 999,
				minLimit: 0,
			});
		}
		if (prevProps.cart.list !== this.props.cart.list && !isEmpty(this.props.cart.list)) {
			let subtotal = 0;
			this.props.cart.list.map((e, i) => {
				let harga = e.details.price * e.quantity;
				subtotal += harga;
			});

			this.setState({
				subtotal,
			});
		}
		if (isEmpty(prevProps.business.promo) && !isEmpty(this.props.business.promo)) {
			this.setState({
				isPromo: true,
			});
		}
		if (isEmpty(prevProps.notification.redirect) && !isEmpty(this.props.notification.redirect)) {
			setTimeout(() => {
				let { redirect } = this.props.notification;
				let urlParams = new URLSearchParams(window.location.search);
				let oID = urlParams.get("order_ref");

				if (redirect === "setItemOfOrder" || redirect === "createOrder") {
					let data = {
						promo_code: !isEmpty(this.props.business.promo) ? this.props.business.promo.code : "",
					};

					this.props.editOrderPromo(data, oID);
				}
				if (redirect === "editOrderPromo") {
					let data = {
						notes: this.state.notes,
					};

					this.props.editOrderNotes(data, oID);
				}
				if (redirect === "editOrderNotes") {
					this.props.setLoading(false);

					window.location.href = `${
						process.env.NODE_ENV === "production" ? "https://checkout.storeup.io" : "http://localhost:3002"
					}/${oID}?r=1`;
				}
			}, 300);
		}
	}

	handleVocer = value => () => {
		this.setState({
			isVocer: value,
		});
	};

	handleCounter = (action, id) => () => {
		let { counter, minLimit, maxLimit } = this.state;
		let isCannotAdd = this.state.maxLimit <= 0 || this.state.minLimit > this.state.maxLimit;
		let numDecrease = counter - 1;
		let numIncrease = counter + 1;
		const minusAction = action === "-";
		const addAction = action === "+";

		this.setState(
			{
				counter:
					minusAction && counter > 0
						? numDecrease >= minLimit
							? numDecrease
							: numDecrease < minLimit
							? 0
							: counter
						: addAction && !isCannotAdd
						? counter <= 0
							? minLimit
							: numIncrease <= maxLimit
							? numIncrease
							: counter
						: counter,
			},
			() => {
				//console.log(this.state.counter, this.state.minLimit, this.state.maxLimit);
			}
		);
	};

	handleText = name => event => {
		let val = event.target.value;
		this.setState({
			[name]: val,
		});
	};

	handlePage = link => () => {
		if (link.includes("http")) {
			window.open(link);
		} else {
			if (this.props.router.asPath !== link) {
				this.props.setLoading(true);
			}

			this.props.router.push(link, undefined, { shallow: true });
		}
	};

	handleSubmitVocer = () => () => {
		if (isEmpty(this.state.promo)) {
			this.props.customNotification("Invalid Promo Code", "error");
			// alert("Promo Wajib diisi");
		} else {
			let subtotal = 0;
			this.props.cart.list.map((e, i) => {
				let harga = e.details.price * e.quantity;

				subtotal += harga;
			});

			this.props.getBusinessPromo(this.props.business.detail.id, this.state.promo, subtotal);
		}
	};

	handleCancelVocer = () => () => {
		this.setState({
			isVocer: false,
			isPromo: false,
			promo: "",
		});

		this.props.getBusinessPromo(this.props.business.detail.id, "");
	};

	handleProductInfo = cartItem => () => {
		const item = this.props.business.listings.find(e => {
			return e.ref == cartItem.item_id;
		});

		this.setState(
			{
				productCardInfo: item,
				clickedVariant: cartItem.variant_number,
			},
			() => {
				this.handleVariantChange(cartItem.variant_number)();
			}
		);
	};

	handleVariantChange = id => () => {
		let varInfo = this.state.productCardInfo.variants.find(v => v.id == id);
		let productLimit = productLimiter(varInfo.min_limit, varInfo.max_limit, varInfo.stock);

		let itemInCart = this.props.cart.list.find(
			c => c.item_id === this.state.productCardInfo.ref && c.variant_number == id
		);

		this.setState({
			selectedVariant: id,
			counter: !isEmpty(itemInCart)
				? itemInCart.quantity
				: productLimit.max <= 0 || productLimit.min > productLimit.max
				? 0
				: productLimit.min,
			maxLimit: productLimit.max,
			minLimit: productLimit.min,
		});
	};

	handleRemoveItem = itm => () => {
		this.props.setCartItem(this.props.cart.list, this.props.business.detail.id, [
			{
				details: itm.details,
				image: itm.image[0].url_s,
				item_id: itm.item_id,
				quantity: 0,
				variant_number: itm.variant_number,
				variant_name: itm.variant_name,
			},
		]);
	};

	handleSubmitProduct = itm => () => {
		let { images, variants, ...details } = itm;
		let { clickedVariant, selectedVariant, counter } = this.state;
		// console.log(clickedVariant, selectedVariant);

		let newCartItem = [
			{
				details: details,
				image: images[0].url_s,
				item_id: details.ref,
				quantity: counter,
				variant_number: selectedVariant,
				variant_name: variants.find(v => v.id == selectedVariant).name,
				available: true,
			},
		];
		if (clickedVariant !== selectedVariant) {
			newCartItem.push({
				details: details,
				image: images[0].url_s,
				item_id: details.ref,
				quantity: 0,
				variant_number: clickedVariant,
				variant_name: variants.find(v => v.id == clickedVariant).name,
			});
		}
		this.props.setCartItem(this.props.cart.list, this.props.business.detail.id, newCartItem, true);

		this.setState({
			productCardInfo: null,
		});
	};

	handleSubmit = () => {
		let products = [];

		this.props.cart.list.map(c => {
			products.push({
				product_ref: c.item_id,
				variant_id: c.variant_number,
				quantity: c.quantity,
			});
		});
		let data = {
			products,
			business_id: this.props.business.detail.id,
		};

		const urlParams = new URLSearchParams(window.location.search);
		let oID = urlParams.get("order_ref");

		if (!isEmpty(oID)) {
			this.props.setItemOfOrder(data, oID);
		} else {
			this.props.createOrder(data, this.props.customer.sessionID);
		}
	};

	render() {
		const { classes, business, cart } = this.props;
		const { isVocer, counter, promo, notes, isPromo, subtotal, productCardInfo, selectedVariant } = this.state;

		const ProductInfo = ({
			name,
			variant,
			price,
			another_price,
			image,
			unavailable,
			ref,
			quantity,
			onRemove,
			onClickItem,
		}) => {
			return (
				<div className={classes.productBox}>
					<div
						style={{
							display: "flex",
							justifyContent: "center",
							position: "relative",
							width: "80px",
							height: "80px",
							marginRight: "15px",
						}}
					>
						<div
							className={
								unavailable ? classes.productCardImgContainerUn : classes.productCardImgContainer
							}
						>
							<img src={image} className={classes.image} />
						</div>
						{!isEmpty(price) && !isEmpty(another_price) ? (
							<div className={unavailable ? classes.cardDiscountedPriceUn : classes.cardDiscountedPrice}>
								<Typography style={{ fontSize: "10px" }}>
									{parseInt(((another_price - price) / another_price) * 100)}% OFF
								</Typography>
							</div>
						) : null}
					</div>
					<div className={classes.productInfo}>
						<div
							style={{
								display: "flex",
								flexDirection: "column",
								justifyContent: "space-between",
								width: "calc(100% - 110px)",
							}}
						>
							<div
								style={{
									display: "flex",
									flexDirection: "column",
									alignItems: "center",
								}}
							>
								<Typography noWrap className={unavailable ? classes.titleUn : classes.title}>
									{name}
								</Typography>

								{!isEmpty(variant) ? (
									<Typography className={unavailable ? classes.variantUn : classes.variant}>
										{variant}
									</Typography>
								) : null}
							</div>
							{unavailable ? (
								<div
									style={{
										padding: "3px 7px",
										boxSizing: "border-box",
										backgroundColor: black,
										borderRadius: "10px",
										color: "white",
										fontSize: "9px",
										alignSelf: "flex-start",
									}}
								>
									Unavailable
								</div>
							) : (
								<div
									className={classes.counterClickerContainer}
									style={{ height: "28px", marginBottom: "2px" }}
									onClick={unavailable ? null : onClickItem}
								>
									<div className={classes.counterIconContainer}>
										<RemoveRounded className={classes.counterIcon} />
									</div>
									<Typography className={classes.counterText}>{quantity}</Typography>
									<div className={classes.counterIconContainer}>
										<AddRounded className={classes.counterIcon} />
									</div>
								</div>
							)}
						</div>

						<div
							style={{
								display: "flex",
								flexDirection: "column",
								justifyContent: "space-between",
							}}
						>
							<div>
								{!isEmpty(price) ? (
									<Typography className={unavailable ? classes.priceUn : classes.price}>
										RM{price.toFixed(2)}
									</Typography>
								) : null}
								{!isEmpty(another_price) ? (
									<Typography className={classes.another_price}>
										RM{another_price.toFixed(2)}
									</Typography>
								) : null}
							</div>

							{unavailable ? (
								<Typography
									onClick={onRemove}
									style={{
										cursor: "pointer",
										textAlign: "right",
										textDecoration: "underline",
										color: errorColor,
										fontWeight: 500,
									}}
								>
									Remove
								</Typography>
							) : null}
						</div>
					</div>
				</div>
			);
		};

		const StoreUpTag = () => {
			return (
				<div
					style={{
						display: "flex",
						position: "absolute",
						bottom: "10px",
						left: "50%",
						transform: "translateX(-50%)",
						cursor: "pointer",
					}}
					onClick={() => {
						window.open("https://storeup.io");
					}}
				>
					<Typography style={{ fontWeight: 300 }}>Powered by</Typography>
					<Typography style={{ fontWeight: 700, marginLeft: "0.3rem" }}>StoreUp</Typography>
				</div>
			);
		};

		const InfoHarga = ({ name, price, isTotal }) => {
			return (
				<div
					style={{
						display: "flex",
						width: "100%",
						justifyContent: "space-between",
						marginBottom: isTotal ? "10px" : "3px",
					}}
				>
					<Typography style={{ fontSize: "14px", color: black, fontWeight: isTotal ? 600 : 400 }}>
						{name}
					</Typography>
					<Typography
						style={{
							fontSize: isTotal ? "28px" : "14px",
							color: black,
							fontWeight: isTotal ? 600 : 500,
						}}
					>
						{price}
					</Typography>
				</div>
			);
		};

		return (
			<div style={{ position: "relative" }}>
				<div className={classes.root}>
					{isEmpty(cart.list) ? (
						<div
							style={{
								position: "absolute",
								top: "230px",
								alignItems: "center",
								display: "flex",
								flexDirection: "column",
								justifyContent: "center",
								width: "100%",
							}}
						>
							<Typography style={{ fontSize: "20px", fontWeight: "500", marginBottom: "5px" }}>
								Empty bag
							</Typography>
							<Typography style={{ fontSize: "15px", marginBottom: "20px", color: greyblack }}>
								No products in the bag at the moment.
							</Typography>
							<Button
								variant="contained"
								style={{ backgroundColor: secondary, color: "white" }}
								onClick={this.handlePage("/")}
							>
								Continue Shopping
							</Button>
						</div>
					) : (
						<div>
							<div style={{ display: "flex" }}>
								<img src="/images/storeuplogo.svg" width="20px" />
								<Typography style={{ fontSize: "24px", fontWeight: "500", marginLeft: "5px" }}>
									Bag
								</Typography>
							</div>
							<Typography className={classes.bag}>Review items in bag</Typography>
							<div>
								<Typography className={classes.product}>PRODUCTS</Typography>
								{this.props.cart.list.map((e, i) => {
									return (
										<ProductInfo
											key={i}
											name={e.details?.name}
											variant={e.variant_name}
											price={e.details?.price}
											another_price={e.details?.another_price}
											image={e.image}
											unavailable={!e.available}
											id={e.details?.id}
											quantity={e.quantity}
											onRemove={this.handleRemoveItem(e)}
											onClickItem={this.handleProductInfo(e)}
										/>
									);
								})}
							</div>
							<div className={classes.boxTextarea}>
								<TextField
									label="Order notes (optional)"
									placeholder="Optional"
									multiline
									rows={6}
									variant="outlined"
									color="secondary"
									style={{ width: "100%" }}
									className={classes.input}
									value={notes}
									onChange={this.handleText("notes")}
								/>
							</div>
							<div
								style={{ width: "100%", textAlign: isVocer ? "left" : "right", marginBottom: "250px" }}
							>
								{isVocer ? (
									isPromo ? (
										<div
											style={{
												display: "flex",
												width: "100%",
												justifyContent: "space-between",
												alignItems: "center",
											}}
										>
											<div className={classes.boxPromo}>
												<Typography style={{ fontSize: "16px", color: black }}>
													{business.promo.code}
												</Typography>
												{!isEmpty(business.promo.value_percent) ? (
													<div className={classes.cardDiscountedPromo}>
														{business.promo.value_percent}% OFF
													</div>
												) : (
													<div className={classes.cardDiscountedPromo}>
														RM{business.promo.value_fixed} OFF
													</div>
												)}
											</div>
											<Typography
												style={{
													fontSize: "1.3rem",
													color: "#FF9500",
													fontWeight: 500,
													cursor: "pointer",
												}}
												onClick={this.handleCancelVocer()}
											>
												Remove
											</Typography>
										</div>
									) : (
										<div
											style={{
												display: "flex",
												width: "100%",
												justifyContent: "space-between",
												alignItems: "center",
											}}
										>
											<TextField
												variant="outlined"
												color="secondary"
												style={{ width: "calc(100% - 100px)" }}
												className={classes.input}
												placeholder="Enter voucher code here"
												value={promo}
												onChange={this.handleText("promo")}
											/>

											<Typography
												style={{
													fontSize: "1.3rem",
													color: "#FF9500",
													fontWeight: 500,
													cursor: "pointer",
												}}
												onClick={this.handleSubmitVocer()}
											>
												Apply
											</Typography>
										</div>
									)
								) : (
									<Typography
										style={{
											fontSize: "14px",
											color: "#FF9500",
											fontWeight: 500,
											cursor: "pointer",
										}}
										onClick={this.handleVocer(true)}
									>
										Apply a voucher
									</Typography>
								)}
							</div>

							<Backdrop open={!isEmpty(productCardInfo)} style={{ zIndex: 9999 }}>
								<Slide direction="up" in={!isEmpty(productCardInfo)} mountOnEnter unmountOnExit>
									{isEmpty(productCardInfo) ? (
										<div />
									) : (
										<div
											style={{
												width: "100%",
												maxWidth: "600px",
												zIndex: 999,
												// height: "300px",
												backgroundColor: "white",
												position: "absolute",
												bottom: 0,

												borderRadius: "15px 15px 0px 0px",
												padding: "20px 30px",
												boxSizing: "border-box",
											}}
										>
											<div
												style={{
													width: "100%",
													display: "flex",
													paddingBottom: "30px",
													boxSizing: "border-box",
													borderBottom: "1px solid",
													borderBottomColor: greywhite,
													marginBottom: "15px",
													alignItems: "center",
													position: "relative",
													// backgroundColor: "red",
												}}
											>
												<div
													style={{
														display: "flex",
														justifyContent: "center",
														position: "relative",
														width: "100px",
														height: "100px",
														marginRight: "15px",
													}}
												>
													<div className={classes.productCardImgContainer}>
														<img
															src={productCardInfo.images[0].url_s}
															className={classes.image}
														/>
													</div>
													{!isEmpty(productCardInfo.price) &&
													!isEmpty(productCardInfo.another_price) ? (
														<div className={classes.cardDiscountedPrice}>
															<Typography style={{ fontSize: "10px" }}>
																{parseInt(
																	((productCardInfo.another_price -
																		productCardInfo.price) /
																		productCardInfo.another_price) *
																		100
																)}
																% OFF
															</Typography>
														</div>
													) : null}
												</div>
												<div>
													<Typography style={{ fontSize: "20px", color: black }}>
														{productCardInfo.name}
													</Typography>
													<div style={{ display: "flex", alignItems: "center" }}>
														{!isEmpty(productCardInfo.price) ? (
															<Typography
																style={{
																	color: grey,
																	fontSize: "1.2rem",
																}}
															>
																RM{productCardInfo.price.toFixed(2)}
															</Typography>
														) : null}
														{!isEmpty(productCardInfo.another_price) ? (
															<Typography
																style={{
																	fontSize: "1.1rem",
																	marginLeft: "7px",
																	color: grey,
																	textDecoration: "line-through",
																}}
															>
																RM{productCardInfo.another_price.toFixed(2)}
															</Typography>
														) : null}
													</div>
												</div>
												<div
													style={{
														padding: "3px",
														width: "20px",
														height: "20px",
														borderRadius: "50%",
														backgroundColor: greywhite,
														position: "absolute",
														right: 0,
														top: 0,
														cursor: "pointer",
													}}
													onClick={() => {
														this.setState({
															productCardInfo: null,
														});
													}}
												>
													<Close color={grey} />
												</div>
											</div>
											{productCardInfo.variants.length > 1 ? (
												<div
													style={{
														borderBottom: "1px solid",
														borderBottomColor: greywhite,
														marginBottom: "15px",
														paddingBottom: "20px",
														boxSizing: "border-box",
													}}
												>
													<Typography
														style={{ fontSize: "16px", color: grey, marginBottom: "8px" }}
													>
														Select variant
													</Typography>
													<div style={{ display: "flex" }}>
														{productCardInfo.variants.map((v, i) => {
															return (
																<div
																	key={i}
																	onClick={this.handleVariantChange(v.id)}
																	className={
																		selectedVariant == v.id
																			? classes.boxVarianActive
																			: classes.boxVarian
																	}
																>
																	{v.name}
																</div>
															);
														})}
													</div>
												</div>
											) : null}
											<div
												style={{
													display: "flex",
													alignItems: "center",
													justifyContent: "space-between",
													marginBottom: "60px",
												}}
											>
												<Typography
													style={{ fontSize: "16px", color: grey, marginBottom: "8px" }}
												>
													Quantity
												</Typography>
												<div className={classes.counterClickerContainer}>
													<div
														className={classes.counterIconContainer}
														onClick={this.handleCounter("-")}
													>
														<RemoveRounded className={classes.counterIcon} />
													</div>
													<Typography className={classes.counterText}>{counter}</Typography>
													<div
														className={classes.counterIconContainer}
														onClick={this.handleCounter("+")}
													>
														<AddRounded className={classes.counterIcon} />
													</div>
												</div>
											</div>
											<div
												style={{
													width: "100%",
													padding: "10px 0",
													textAlign: "center",
													borderRadius: "20px",
													fontSize: "16px",
													backgroundColor: "blue",
													color: "white",
													cursor: "pointer",
												}}
												onClick={this.handleSubmitProduct(productCardInfo)}
											>
												Confirm
											</div>
											<div
												style={{
													marginTop: "15px",
													display: "flex",
													width: "100%",
													justifyContent: "center",
													cursor: "pointer",
												}}
												onClick={() => {
													window.open("https://storeup.io");
												}}
											>
												<Typography style={{ fontWeight: 300 }}>Powered by</Typography>
												<Typography style={{ fontWeight: 700, marginLeft: "0.3rem" }}>
													StoreUp
												</Typography>
											</div>
										</div>
									)}
								</Slide>
							</Backdrop>
						</div>
					)}
				</div>
				{isEmpty(this.props.cart.list) ? null : (
					<div
						style={{
							boxSizing: "border-box",
							padding: "10px 18px 50px",
							boxShadow: "0px -1px 3px rgba(0, 0, 0, 0.1)",
							width: "100%",
							maxWidth: "600px",
							position: "fixed",
							zIndex: 150,
							bottom: 0,
							backgroundColor: "white",
						}}
					>
						{/* {this.props.cart.list.map} */}
						{/* <InfoHarga name="Subtotal" price="RM155.00" /> */}
						<InfoHarga name="Subtotal" price={`RM${subtotal.toFixed(2)}`} />
						<InfoHarga name="Shipping" price="-.--" />
						{isPromo && (
							<InfoHarga
								name={`Promo (${business.promo.code})`}
								price={`-RM${business.promo.amount.toFixed(2)}`}
							/>
						)}
						{isPromo ? (
							<InfoHarga
								name="Total"
								price={`RM${(subtotal - business.promo.amount).toFixed(2)}`}
								isTotal
							/>
						) : (
							<InfoHarga name="Total" price={`RM${subtotal.toFixed(2)}`} isTotal />
						)}
						<button className={classes.btn} onClick={this.handleSubmit}>
							Checkout
						</button>
						<StoreUpTag />
					</div>
				)}
			</div>
		);
	}
}

const styles = theme => ({
	root: {
		display: "flex",
		flexDirection: "column",
		width: "100%",
		maxWidth: "600px",
		alignSelf: "center",
		[theme.breakpoints.down("xs")]: {
			padding: "0 12px",
			boxSizing: "border-box",
		},
	},
	boxVarian: {
		padding: "5px 8px",
		boxSizing: "border-box",
		border: "1px solid blue",
		color: "blue",
		borderRadius: "5px",
		cursor: "pointer",
		marginRight: "10px",
	},
	boxVarianActive: {
		padding: "5px 8px",
		boxSizing: "border-box",
		backgroundColor: "blue",
		color: "white",
		borderRadius: "5px",
		cursor: "pointer",
		marginRight: "10px",
	},
	cardDiscountedPromo: {
		background: "white",
		border: "1px solid #FF9500",
		borderRadius: "20px",
		textAlign: "center",
		color: "#FF9500",
		height: "19px",
		width: "75px",
		fontSize: "14px",
		marginLeft: "35px",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
	},
	btn: {
		width: "100%",
		height: "46px",
		textAlign: "center",
		border: "none",
		backgroundColor: "#007AFE",
		borderRadius: "22px",
		color: "white",
		fontSize: "16px",
		cursor: "pointer",
	},
	input: {
		fontSize: "1.2rem",
	},
	boxPromo: {
		width: "calc(100% - 130px)",
		height: "30px",
		fontSize: "1.2rem",
		// padding: "12px 20px",
		padding: "15px 20px",
		border: "1px solid #ccc",
		borderRadius: "4px",
		display: "flex",
		// alignItems:
	},
	boxTextarea: {
		paddingTop: "25px",
		borderTop: "1px solid #ccc",
		marginBottom: "35px",
		width: "100%",
	},
	textarea: {
		width: "100%",
		height: "150px",
		border: "1px solid #ccc",
		padding: "12px 20px",
		boxSizing: "border-box",
		borderRadius: "4px",
		resize: "none",
		fontSize: "1.2rem",
	},
	bag: {
		fontSize: "16px",
		color: greyblack,
		marginBottom: "22px",
	},
	product: {
		color: greyblack,
		fontSize: "1.3rem",
		fontWeight: 500,
		marginBottom: "15px",
	},
	productBox: {
		display: "flex",
		width: "100%",
		marginBottom: "36px",
	},
	productCardImgBox: {
		width: "80px",
		height: "80px",
	},
	productCardImgContainer: {
		border: "1px solid gainsboro",
		position: "relative",
		borderRadius: "15px",
		overflow: "hidden",
		width: "100%",
		height: "100%",
	},
	productCardImgContainerUn: {
		filter: "grayscale(1)",
		border: "1px solid gainsboro",
		position: "relative",
		borderRadius: "15px",
		overflow: "hidden",
		width: "100%",
		height: "100%",
	},
	image: {
		width: "100%",
		height: "100%",
		objectFit: "cover",
	},
	cardDiscountedPrice: {
		background: "#FF9500",
		borderRadius: "5px",
		textAlign: "center",
		color: "white",
		position: "absolute",
		height: "14px",
		width: "55px",
		borderRadius: "6px",
		bottom: -7,
		zIndex: 10,
	},
	cardDiscountedPriceUn: {
		backgroundColor: greydark,
		// opacity: "0.3",
		borderRadius: "5px",
		textAlign: "center",
		color: "white",
		position: "absolute",
		height: "14px",
		width: "55px",
		borderRadius: "6px",
		bottom: -7,
		zIndex: 10,
	},
	productInfo: {
		display: "flex",
		justifyContent: "space-between",
		width: "calc(100% - 80px)",
		// backgroundColor: "blue",
	},
	title: {
		width: "100%",
		fontSize: "1.2rem",
		fontWeight: 500,
		color: black,
	},
	titleUn: {
		width: "100%",
		fontSize: "1.2rem",
		fontWeight: 500,
		color: greydark,
	},
	variant: {
		width: "100%",
		fontSize: "0.9rem",
		color: greyblack,
	},
	variantUn: {
		width: "100%",
		fontSize: "0.9rem",
		color: grey,
	},
	price: {
		color: black,
		fontWeight: "bold",
		fontSize: "1.2rem",
		textAlign: "right",
	},
	priceUn: {
		opacity: "0.5",
		color: "#777977",
		fontWeight: "bold",
		fontSize: "1.2rem",
		textAlign: "right",
	},
	another_price: {
		fontSize: "1rem",
		opacity: "0.5",
		color: "#777977",
		textAlign: "right",
		textDecoration: "line-through",
	},
	counterClickerContainer: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		padding: "0.3rem",
		boxSizing: "border-box",
		width: "120px",
		backgroundColor: "gainsboro",
		height: "34px",
		borderRadius: "calc(20px + 0.4rem)",
	},
	counterClickerContainerUn: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		padding: "0.3rem",
		boxSizing: "border-box",
		width: "120px",
		backgroundColor: "gainsboro",
		opacity: "0.3",
		height: "34px",
		borderRadius: "calc(20px + 0.4rem)",
		cursor: "not-allowed",
	},
	counterIconContainer: {
		transition: "all 0.3s",
		cursor: "pointer",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		height: "30px",
		width: "30px",
		borderRadius: "20px",
	},
	counterIconContainerUn: {
		transition: "all 0.3s",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		height: "30px",
		width: "30px",
		borderRadius: "20px",
	},
	counterIcon: {
		color: secondary,
		fontSize: "1.3rem",
		fontWeight: "bold",
	},
	counterText: {
		fontSize: "1.2rem",
		color: "black",
		fontWeight: "bold",
		padding: "0 2rem",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	business: state.business,
	booking: state.booking,
	item: state.item,
	cart: state.cart,
	customer: state.customer,
	notification: state.notification,
	//   loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, {
	setLoading,
	customNotification,
	getBookDetails,
	getBusinessPromo,
	setCartItem,
	createOrder,
	setItemOfOrder,
	editOrderPromo,
	editOrderNotes,
	getOrderBySessionID,
})(withRouter(withStyles(styles)(Bag)));
