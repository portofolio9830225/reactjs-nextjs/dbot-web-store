import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "next/router";
import PageHelmet from "../components/mini/PageHelmet";
import isEmpty from "../utils/isEmpty";

import { setCartItem } from "../store/actions/cartAction";
import { clearItem, getItemDetails } from "../store/actions/itemAction";
import { customNotification, clearNotification } from "../store/actions/notificationAction";

import { withMobileDialog, Typography, Button, Dialog, InputBase, Collapse, Fade } from "@material-ui/core";

import Loading from "../components/mini/Loading";
import SwipeableSection from "../components/mini/SwipeableSection";
import { AddRounded, CloseRounded, RemoveRounded, SelectAllRounded } from "@material-ui/icons";
import { greydark, grey, mainBgColor, errorColor, secondary } from "../utils/ColorPicker";
import NotFound from "../components/NotFound";
import ImageGrid from "../components/mini/ImageGrid";
import { setLoading } from "../store/actions/loadingAction";
import productLimiter from "../utils/productLimiter";

const styles = theme => ({
	root: {
		display: "flex",
		flexDirection: "column",
		width: "100%",
		maxWidth: "600px",
		alignSelf: "center",
	},
	container: {
		display: "flex",
		flexDirection: "column",
		width: "100%",
		position: "relative",
		alignSelf: "center",
	},
	image: {
		width: "100%",
		height: "100%",
		objectFit: "cover",
	},
	main: {
		alignSelf: "center",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		width: "100%",
		marginBottom: "3rem",
		marginTop: "3rem",
		[theme.breakpoints.down("xs")]: {
			width: "90%",
			marginTop: 0,
		},
	},
	productCard: {
		marginTop: "2rem",
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		borderRadius: "15px",
		boxShadow: "5px 5px 8px #e3eeff, -5px -5px 5px #ffffff",
		overflow: "hidden",
		backgroundColor: mainBgColor,
	},
	productCardImgContainer: {
		position: "relative",
		display: "flex",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		width: "100%",
		height: "100%",
		maxWidth: "600px",
		maxHeight: "500px",
	},

	productCardTextContainer: {
		width: "100%",
		boxSizing: "border-box",
		padding: "1rem 0",
		display: "flex",
		flexDirection: "column",
	},
	productCardActionContainer: {
		marginTop: "0.5rem",
		width: "100%",
		display: "flex",
		flexDirection: "row",
		// justifyContent: "space-between",
		alignItems: "center",
	},

	title: {
		fontSize: "2.3rem",
		fontWeight: 500,
		letterSpacing: "0.5px",
		width: "100%",
	},
	lDescTitle: {
		fontWeight: 700,
		marginTop: "1rem",
		width: "100%",
	},

	lDescText: {
		fontSize: "1.3rem",
		marginTop: "0.5rem",
		width: "100%",
		fontWeight: 300,
		letterSpacing: "0.2px",
		padding: 0,
	},
	price: {
		fontSize: "1.7rem",
		color: greydark,
		marginRight: "10px",
	},
	another_price: {
		fontSize: "1.7rem",
		opacity: "0.5",
		color: "#777977",
		textDecoration: "line-through",
	},
	subtotalCounterContainer: {
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
	},
	subtotalContainer: {
		display: "flex",
		flexDirection: "column",
	},
	subtotalTitle: {
		color: greydark,
		fontSize: "0.8rem",
	},
	subtotalPrice: {
		fontSize: "2rem",
		fontWeight: 500,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.7rem",
		},
	},
	counterClickerContainer: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		padding: "0.3rem",
		boxSizing: "border-box",
		//margin: "auto",
		//width: "100%",
		minWidth: "125px",
		//backgroundColor: "#eeeef0",
		backgroundColor: "gainsboro",
		//boxShadow: "inset 2px 2px 4px #d6d6e3, inset -2px -2px 4px #ffffff",
		height: "calc(40px + 0rem)",
		borderRadius: "calc(20px + 0.4rem)",
	},
	stockText: {
		alignSelf: "flex-end",
		fontWeight: 300,
		fontSize: "1rem",
		color: grey,
		//paddingRight: "0.5rem",
		paddingTop: "0.3rem",
	},
	variantText: {
		marginBottom: "0.7rem",
		fontWeight: 400,
		fontSize: "1.1rem",
		color: greydark,
		//paddingRight: "0.5rem",
		paddingTop: "0.3rem",
	},
	counterText: {
		fontSize: "1.2rem",
		color: "black",
		fontWeight: "bold",
		//textShadow: "1px 1px 2px #999, -1px -1px 2px #ffffff",
		padding: "0 2rem",
		[theme.breakpoints.down("xs")]: {
			padding: 0,
		},
	},

	counterIconContainer: {
		transition: "all 0.3s",
		cursor: "pointer",

		//boxShadow:
		// "-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		height: "40px",
		width: "40px",
		borderRadius: "20px",
		// background: "linear-gradient(145deg, #fcfdff, #dbdbe8)",
		// "&:active": {
		//   background: "linear-gradient(145deg, #dbdbe8, #fcfdff)",
		// },
	},
	counterIcon: {
		color: secondary,
		fontSize: "1.3rem",
		fontWeight: "bold",
	},
	dividerGrey: {
		width: "100%",
		borderBottom: "0.5px solid gainsboro",
	},
	sectionTitle: {
		alignSelf: "flex-start",
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		marginBottom: "0.5rem",
	},
	sectionTitleText: {
		marginLeft: "0.8rem",
		color: greydark,
		textTransform: "uppercase",
		fontSize: "1.3rem",
		fontWeight: 500,
		width: "100%",

		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
			marginBottom: 0,
		},
	},
	backIconContainer: {
		transition: "all 0.3s",
		cursor: "pointer",
		boxShadow:
			"-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
		// boxShadow: "2px 2px 4px #e3eeff, -2px -2px 4px #ffffff",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		height: "40px",
		minWidth: "40px",
		borderRadius: "50%",
		background: "linear-gradient(145deg, #fcfdff, #dbdbe8)",
		"&:active": {
			background: "linear-gradient(145deg, #dbdbe8, #fcfdff)",
		},
	},
	backIcon: {
		color: greydark,
		fontSize: "2rem",
	},
	textField: {
		width: "100%",
		marginTop: "1.5rem",
	},
	orderCard: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		position: "fixed",
		bottom: 0,
		// left: 0,
		width: "100%",
		maxWidth: "600px",
		zIndex: 100,
		boxShadow: "0px -2px 2px rgba(0, 0, 0, 0.1)",
		borderTop: "1px solid gainsboro",
	},
	orderCardBackground: {
		display: "flex",
		width: "100%",
		backgroundColor: mainBgColor,
		boxShadow: "0px -2px 2px rgba(0,0,0,0.03)",
		// backgroundColor: "transparent",
	},
	orderCardContent: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "space-between",
		alignItems: "center",
		margin: "auto",
		width: "100%",
		padding: "1rem 16px",
		[theme.breakpoints.down("xs")]: {
			width: "90%",
		},
	},
	priceList: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		width: "100%",
		marginBottom: "1rem",
	},

	BankMenuItem: {
		paddingTop: "1.5rem",
		paddingBottom: "1.5rem",
	},
	DialogRoot: {
		backgroundColor: "rgba(0,0,0,0)",
		boxShadow: "none",
		display: "flex",
		borderRadius: 0,
	},
	maxLimitText: {
		fontWeight: 500,
		color: errorColor,
		textAlign: "right",
		letterSpacing: "0.3px",
		marginRight: "0.8rem",
		// paddingTop: "0.5rem",
		paddingBottom: "0.5rem",
		fontSize: "1rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "0.9rem",
		},
	},
});

class ItemPage extends Component {
	state = {
		isReady: false,
		itmID: null,
		step: null,
		counter: 0,
		isCounterAction: false,
		minLimit: 0,
		maxLimit: 999,
		//isInCart: false,
		isCannotAdd: false,
		selectedVariant: null,
		selectedVariantName: null,
	};

	componentDidMount() {
		// console.log(this.props.SEO);
		this.props.setLoading(false);
		this.props.getItemDetails(this.props.router.query.productID, this.props.business.detail.id);
	}

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.item.itemPage.details) && !isEmpty(this.props.item.itemPage.details)) {
			let { details, total_stock, variants } = this.props.item.itemPage;

			this.setState(
				{
					itmID: details.item_id,
					step: 1,
					selectedVariant: variants[0].id,
					selectedVariantName: variants[0].name,
				},
				() => {
					let foundItem = this.props.cart.list.find(
						f =>
							f.variant_number == this.state.selectedVariant &&
							f.item_id == this.props.router.query.productID
					);

					let productLimit = productLimiter(
						variants[0].min_limit,
						variants[0].max_limit,
						variants[0].stock,
						!isEmpty(foundItem) ? foundItem.quantity : 0
					);

					this.setState(
						{
							counter:
								productLimit.max <= 0 || productLimit.min > productLimit.max ? 0 : productLimit.min,
							minLimit: productLimit.min,
							maxLimit: productLimit.max,
							isCannotAdd: productLimit.max <= 0 || productLimit.min > productLimit.max,
						},
						() => {
							this.setState({
								isReady: true,
							});
						}
					);
				}
			);
		}
	}

	componentWillUnmount() {
		this.props.clearItem();
	}

	handleCounter = action => () => {
		let { counter, minLimit, maxLimit, isCannotAdd } = this.state;
		let numDecrease = counter - 1;
		let numIncrease = counter + 1;
		const minusAction = action === "-";
		const addAction = action === "+";

		this.setState({
			counter:
				minusAction && counter > 0 && numDecrease >= minLimit
					? numDecrease
					: addAction && !isCannotAdd && numIncrease <= maxLimit
					? numIncrease
					: counter,
			// 	action === "-"
			// 		? this.state.counter > 0 && numDecrease >= varFound.min_limit
			// 			? numDecrease
			// 			: 0
			// 		: action === "+"
			// 		? !this.state.isCannotAdd
			// 			? this.state.counter <= 0 && varFound.min_limit
			// 				? varFound.min_limit
			// 				: numIncrease <= this.state.maxLimit
			// 				? numIncrease
			// 				: this.state.counter
			// 			: 0
			// 		: this.state.counter,
		});
	};

	handleStep = step => {
		this.setState({
			step,
		});
	};

	handleVariant = (val, name) => () => {
		this.setState(
			{
				isReady: false,
			},
			() => {
				let foundItem = this.props.cart.list.find(
					f => f.variant_number == val && f.item_id == this.props.router.query.productID
				);

				let varIndex = this.props.item.itemPage.variants.findIndex(f => f.id === val);
				let productLimit = productLimiter(
					this.props.item.itemPage.variants[varIndex].min_limit,
					this.props.item.itemPage.variants[varIndex].max_limit,
					this.props.item.itemPage.variants[varIndex].stock,
					!isEmpty(foundItem) ? foundItem.quantity : 0
				);

				this.setState(
					{
						selectedVariant: val,
						selectedVariantName: name,
						counter:
							productLimit.max <= 0 || productLimit.min > productLimit.max
								? 0
								: productLimit.min > 0
								? productLimit.min
								: 1,
						minLimit: productLimit.min,
						maxLimit: productLimit.max,
						isCannotAdd: productLimit.max <= 0 || productLimit.min > productLimit.max,
					},
					() => {
						this.setState({
							isReady: true,
						});
					}
				);
			}
		);
	};

	handleSubmit = () => {
		this.props.setCartItem(this.props.cart.list, this.props.business.detail.id, [
			{
				details: this.props.item.itemPage.details,
				image: this.props.item.itemPage.images[0].url_s,
				item_id: this.props.router.query.productID,
				quantity: this.state.counter,
				variant_number: this.state.selectedVariant,
				variant_name: this.state.selectedVariantName,
				available: true,
			},
		]);
		this.handleBack();
	};

	handleBack = () => {
		this.props.router.back();
	};

	render() {
		const { classes, SEO, fullScreen } = this.props;
		const { itmID, step, counter, isReady, maxLimit, isCannotAdd, selectedVariant, selectedVariantName } =
			this.state;
		const { itemPage, isItemFetched } = this.props.item;
		const { details, images, shipping, stock, variants } = itemPage;
		const { detail } = this.props.business;
		const loading = this.props.loading.status;
		const isSoldOut =
			!isEmpty(details) &&
			(details.total_stock <= 0 || (!isEmpty(details.min_limit) && details.min_limit > stock));

		const SoldOutTag = props => {
			return (
				<div
					style={{
						width: "100%",
						height: "100%",
						position: "absolute",
						top: 0,
						left: 0,
						display: "flex",
						alignItems: "center",
						justifyContent: "center",
					}}
				>
					<div
						style={{
							width: "250px",
							height: "250px",
							borderRadius: "125px",
							backgroundColor: "rgba(0,0,0,0.5)",
							display: "flex",
							alignItems: "center",
							justifyContent: "center",
						}}
					>
						<Typography
							style={{
								color: "white",
								fontSize: "2.2rem",
								letterSpacing: "1px",
							}}
						>
							Sold Out
						</Typography>
					</div>
				</div>
			);
		};

		const VariantBox = props => {
			let active = selectedVariant === props.val;
			return (
				<div
					onClick={props.onSelect}
					style={{
						cursor: "pointer",
						backgroundColor: active ? secondary : mainBgColor,
						color: active ? mainBgColor : secondary,
						border: `1px solid ${active ? mainBgColor : secondary}`,
						borderRadius: "0.5rem",
						marginRight: "1rem",
						padding: "0.3rem 0.5rem",
						transition: "all 0.3s",
					}}
				>
					<Typography>{props.name}</Typography>
				</div>
			);
		};

		return (
			<div className={classes.root}>
				<PageHelmet
					metadata={{
						title: `${SEO.name}`,
						description: `${SEO.description} [Powered by StoreUp]`,
						image: SEO.images[0].url_s,
					}}
				/>

				{/* {loading && (
          <Dialog
            open={loading}
            elevation={0}
            classes={{
              paper: classes.DialogRoot,
            }}
          >
            <Loading open={loading} />
          </Dialog>
        )} */}
				{isItemFetched ? (
					!isEmpty(details) ? (
						<div className={classes.container}>
							{step === 1 && (
								<Fade in={step === 1}>
									<div
										style={{
											maxWidth: "600px",
											alignSelf: "center",
										}}
									>
										<div
											style={{
												position: "fixed",
												top: 0,
												zIndex: 100,
												width: "100%",
												maxWidth: "600px",
												display: "flex",
												justifyContent: "flex-end",
												alignSelf: "center",
											}}
											onClick={this.handleBack}
										>
											<div
												className={classes.counterIconContainer}
												style={{
													marginTop: "1rem",
													marginRight: "1rem",
													boxShadow: "none",
												}}
											>
												<CloseRounded style={{ fontSize: "2rem", color: "black" }} />
											</div>
										</div>

										<ImageGrid images={images} details={details} />
										{/* <SwipeableSection
                    slides={images.map((e, i) => {
                      return (
                        <div
                          key={i}
                          className={classes.productCardImgContainer}
                        >
                          {isSoldOut && <SoldOutTag />}
                          <img
                            src={e.image_url}
                            style={{
                              width: "100%",
                              height: "100%",
                              objectFit: "contain",
                            }}
                          />
                        </div>
                      );
                    })}
                  /> */}
									</div>
								</Fade>
							)}
							{step === 1 && (
								<Fade in={step === 1}>
									<div className={classes.main}>
										<div className={classes.productCardTextContainer}>
											<Typography className={classes.title}>{details.name}</Typography>

											<div className={classes.productCardActionContainer}>
												{!isEmpty(details) ? (
													<Typography className={classes.price}>
														RM{details.price.toFixed(2)}
													</Typography>
												) : null}
												{!isEmpty(details) && !isEmpty(details.another_price) ? (
													<Typography className={classes.another_price}>
														RM{details.another_price.toFixed(2)}
													</Typography>
												) : null}
											</div>
											<Typography className={classes.stockText} style={{ width: "100%" }}>
												Stocks:{" "}
												{!isEmpty(selectedVariant)
													? variants.find(f => f.id == selectedVariant).stock
													: stock}
											</Typography>
											{variants.length > 1 ? (
												<div
													style={{
														display: "flex",
														flexDirection: "row",
														flexWrap: "wrap",
														marginTop: "0.5rem",
														// justifyContent: "space-between",
													}}
												>
													<Typography
														className={classes.variantText}
														style={{ width: "100%" }}
													>
														Select variant
													</Typography>
													{variants.map((e, i) => {
														return (
															<VariantBox
																key={i}
																name={e.name}
																onSelect={this.handleVariant(e.id, e.name)}
																val={e.id}
															/>
														);
													})}
												</div>
											) : null}
											{/* {!isEmpty(details) && !isEmpty(details.min_limit) ? (
													<Typography
														className={classes.stockText}
														// style={{ fontWeight: 400 }}
													>
														, Min. quantity : {details.min_limit}
													</Typography>
												) : null}
												{!isEmpty(details) && !isEmpty(details.max_limit) ? (
													<Typography
														className={classes.stockText}
														// style={{ fontWeight: 400 }}
													>
														, Max. quantity : {details.max_limit}
													</Typography>
												) : null} */}
										</div>
										<div className={classes.dividerGrey} />
										<Typography className={classes.lDescTitle}>About this product</Typography>
										<InputBase
											className={classes.lDescText}
											value={
												!isEmpty(details)
													? !isEmpty(details.description)
														? details.description
														: "No description"
													: ""
											}
											multiline
											readOnly
										/>
									</div>
								</Fade>
							)}
							<div style={{ marginBottom: "14rem" }} />
							<div className={classes.orderCard}>
								<div className={classes.orderCardBackground}>
									<div className={classes.orderCardContent}>
										<Collapse in={counter >= maxLimit || isCannotAdd}>
											<Typography className={classes.maxLimitText}>
												{isCannotAdd ? "Insufficient stock" : "Max. quantity reached"}
											</Typography>
										</Collapse>
										<div className={classes.subtotalCounterContainer}>
											<Fade in={counter > 0}>
												<div className={classes.subtotalContainer}>
													{/* <Typography className={classes.subtotalTitle}>
                          Subtotal
                        </Typography> */}
													<Typography className={classes.subtotalPrice}>
														RM{(details.price * counter).toFixed(2)}
													</Typography>
												</div>
											</Fade>
											<div className={classes.counterClickerContainer}>
												<div
													className={classes.counterIconContainer}
													onClick={isReady ? this.handleCounter("-") : null}
												>
													<RemoveRounded className={classes.counterIcon} />
													{/* </Zoom>
                          )} */}
												</div>
												<Typography className={classes.counterText}>{counter}</Typography>
												<div
													className={classes.counterIconContainer}
													onClick={isReady ? this.handleCounter("+") : null}
												>
													<AddRounded className={classes.counterIcon} />
												</div>
											</div>
										</div>

										{/* <Grow in={!isCounterAction}> */}
										<Button
											onClick={this.handleSubmit}
											disabled={
												isSoldOut ||
												// (counter <= 0 && !this.state.isInCart) ||
												counter <= 0 ||
												(!isEmpty(variants) && isEmpty(selectedVariant))
											}
											variant="contained"
											color="secondary"
											style={{ width: "100%", marginTop: "1.5rem" }}
										>
											Add to Cart
											{/* {this.state.isInCart
												? counter > 0
													? "Update cart"
													: "Remove from cart"
												: "Add to Cart"} */}
										</Button>
										{/* </Grow> */}
									</div>
								</div>
							</div>
						</div>
					) : (
						<NotFound />
					)
				) : (
					<div />
				)}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	item: state.item,
	business: state.business,
	cart: state.cart,
	loading: state.loading,
	notification: state.notification,
	error: state.error,
});

export default connect(mapStateToProps, {
	setCartItem,
	getItemDetails,
	clearItem,
	customNotification,
	clearNotification,
	setLoading,
})(withStyles(styles)(withMobileDialog({ breakpoint: "xs" })(withRouter(ItemPage))));
