import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import PageHelmet from "../components/mini/PageHelmet";
import isEmpty from "../utils/isEmpty";
import { setLoading } from "../store/actions/loadingAction";

class Order extends Component {
	componentDidMount() {
		this.props.setLoading(true);

		let params = this.props.router.asPath.split("?")[1];
		window.location.replace(
			`https://receipt.storeup.io/${this.props.router.query.bID}${!isEmpty(params) ? `?${params}` : ""}`
		);
	}

	render() {
		const { detail } = this.props.business;
		return (
			<div>
				<PageHelmet
					metadata={{
						title: `Order details for ${this.props.router.query.bID} | ${
							!isEmpty(detail) ? `${detail.name}` : "StoreUp"
						}`,
					}}
				/>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	business: state.business,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { setLoading })(withRouter(Order));
