import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import libphonenumber from "google-libphonenumber";
import isEmpty from "../utils/isEmpty";
import { withStyles } from "@material-ui/core/styles";

import { withMobileDialog, withWidth, Typography, InputAdornment, Button, TextField } from "@material-ui/core";

import { setLoading } from "../store/actions/loadingAction";
import TextInput from "../components/mini/TextInput";
import { setInputError } from "../store/actions/errorAction";
import { clearNotification, customNotification } from "../store/actions/notificationAction";
import StoreUpTag from "../components/mini/StoreUpTag";
import { updateSessionInfo } from "../store/actions/customerAction";

const styles = theme => ({
	root: {
		flex: 1,
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		width: "100%",
		position: "relative",
	},
	main: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		marginBottom: "4rem",
		width: "100%",
		height: "100%",
		[theme.breakpoints.down("xs")]: {
			width: "95%",
		},
	},
	businessCard: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		margin: "1.5rem 0",
		width: "100%",
	},
	title: {
		marginTop: "1rem",
		fontSize: "1.7rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.4rem",
			textAlign: "center",
		},
	},
	textField: {
		width: "100%",
		marginTop: "1.5rem",
	},
	submitButton: {
		width: "100%",
		marginTop: "2rem",
	},
});

class Business extends Component {
	state = {
		name: "",
		phone: "",
		concern: "",
	};

	componentDidMount() {
		this.props.setLoading(false);
	}

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.notification.redirect) && this.props.notification.redirect === "sessionInfo") {
			let text = `[  _From:_ *${this.props.business.detail.subdomain}.storeup.site Contact Us Page*  ]\n\nHi ${this.props.business.detail.name}, ${this.state.name} here.\n\n${this.state.concern}`;

			setTimeout(() => {
				//this.props.setLoading(false);
				window.location.assign(`https://wa.me/60${this.props.business.detail.phone}?text=${encodeURI(text)}`);
			}, 1000);
		}
	}

	handleText =
		(name, limit = 999) =>
		event => {
			let val = event.target.value;

			if (val.length <= limit) {
				this.setState({ [name]: val });
			}
		};

	handleSubmit = () => {
		this.props.setInputError();
		this.props.clearNotification();
		let err = [];
		if (isEmpty(this.state.name)) {
			err.push({ param: "name", error: "Missing value" });
		}

		if (isEmpty(this.state.phone)) {
			err.push({ param: "phone", error: "Missing value" });
		} else {
			const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();
			const phoneNum = phoneUtil.parseAndKeepRawInput(this.state.phone, "MY");

			if (
				!phoneUtil.isValidNumberForRegion(phoneNum, "MY") ||
				phoneUtil.getNumberType(phoneNum) != libphonenumber.PhoneNumberType.MOBILE
			) {
				err.push({ param: "phone", error: "Invalid phone number" });
			}
		}

		if (!isEmpty(err)) {
			this.props.customNotification("Invalid information", "error");
			this.props.setInputError(err);
		} else {
			this.props.setLoading(true);
			let data = {
				name: this.state.name,
				phone: this.state.phone,
			};
			this.props.updateSessionInfo(this.props.customer.sessionID, data);
		}
	};

	render() {
		const { classes } = this.props;
		const { name, phone, concern } = this.state;
		const { detail } = this.props.business;

		return (
			<div className={classes.root}>
				<div className={classes.root}>
					<div className={classes.main}>
						<div className={classes.businessCard}>
							<i class="fab fa-whatsapp" style={{ fontSize: "6rem" }} />
							<Typography className={classes.title}>
								Contact us via <span style={{ color: "#25D366" }}>Whatsapp</span>
							</Typography>
						</div>
						<TextInput
							className={classes.textField}
							variant="outlined"
							color="primary"
							label="Full name"
							value={name}
							onChange={this.handleText("name")}
							errorkey="name"
						/>
						<TextInput
							className={classes.textField}
							variant="outlined"
							color="primary"
							label="Phone number"
							placeholder="Your phone number"
							type="number"
							value={phone}
							onChange={this.handleText("phone")}
							InputProps={{
								startAdornment: (
									<InputAdornment position="start">
										<Typography>+60</Typography>
									</InputAdornment>
								),
							}}
							errorkey="phone"
						/>
						<TextInput
							multiline
							rows={3}
							className={classes.textField}
							variant="outlined"
							color="primary"
							label="Your concern"
							placeholder="Optional"
							value={concern}
							onChange={this.handleText("concern")}
						/>
						<Button
							variant="contained"
							color="primary"
							className={classes.submitButton}
							onClick={this.handleSubmit}
						>
							Send Whatsapp
						</Button>
					</div>
					<StoreUpTag />
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	item: state.item,
	business: state.business,
	customer: state.customer,
	booking: state.booking,
	item: state.item,
	cart: state.cart,
	// loading: state.loading,
	notification: state.notification,
	error: state.error,
});

export default connect(mapStateToProps, {
	setInputError,
	updateSessionInfo,
	customNotification,
	clearNotification,
	setLoading,
})(withStyles(styles)(withMobileDialog({ breakpoint: "xs" })(withWidth()(withRouter(Business)))));
