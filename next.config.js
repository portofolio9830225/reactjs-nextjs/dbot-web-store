const generateRoute = require("./script/generateRoute");

module.exports = {
  webpack: function (config, { isServer }) {
    if (!isServer) {
      config.node = {
        fs: "empty",
      };
    } else {
      generateRoute();
    }
    config.module.rules.push({
      test: /\.md$/,
      use: "raw-loader",
    });
    return config;
  },
  async redirects() {
    return [
      {
        source: "/sitemap.xml",
        destination: "/api/sitemap",
        permanent: false,
      },
    ];
  },
};
